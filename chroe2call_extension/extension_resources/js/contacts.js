var newVersion = 0;
var cuntryCode = 'US';
getContactSetVersion = function() {
	var version = localStorage.contactset_version;
	if (!version || version == 'undefined') {
		version = -1;
	}

	return version;
};

syncContacts = function(version, country) {
	// TODO: For now we are re-saving everything. Change this to get only
	// the recently updated contacts and merge with the existing ones.
	url = environment.synccontacts + 0;
	console.log('Starting contact sync from ' + url);
	newVersion = version;
	cuntryCode = country;
	$.get(url)
		.success(rebuildContactDatabase)
		.error(function(e) {console.log('Contact sync failed ' + e);});
};

rebuildContactDatabase = function(data) {
	try {
		var validContacts = new Array();
		$.each(data, function(index, contact) {
			try {
				country = countryForE164Number(contact.phoneNumber);
				
				if (!country) {
					console.log('Ignored. Invalid or short-code number ' + contact.phoneNumber);
				} else {
					contact.label = contact.name;
					contact.value = formatLocal(cuntryCode, contact.phoneNumber);
					contact.desc = contact.type + ": " + formatLocal(cuntryCode, contact.phoneNumber);
					validContacts.push(contact);
				}
			} catch (t) {
				console.log(t);
				// ignore this contact. Nothing we can do.
			}
		});
		
		localStorage.contacts = JSON.stringify(validContacts);
		localStorage.contactset_version = newVersion;
	} catch (e) {
		console.log("error " + e);
		// This way we know that we should re-try next time.
		localStorage.contactset_version = 0;
	}
};

getContacts = function() {
	return JSON.parse(localStorage.contacts);
};