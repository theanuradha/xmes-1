popup = {};

popup.countrycode = 'US';
popup.selectedconversation = null;
popup.new_message = null;

popup.event_enteringphonenumber = function() {
	var formatted = formatLocal(popup.countrycode, $('#phonenumber').val());
	$('#txt_formatted_number').html(formatted);
};

popup.request_success = function(data) {
	show_hide_loading_screen_(false);
	util.assertLoggedIn(data);

	// Log in was successful.
	var phone_list = $("#phone_list");

	if (data.devices && data.devices.length == 0) {
		show_no_devices_msg_();
	} else {
		$.each(data.devices, function(i, device) {
			// Only show authenticated devices
			var deviceCount = 0;
			if (device.authenticated) {
				deviceCount = deviceCount + 1;
				phone_list.append($("<option></option>")
				         .attr("value",device.extensionDeviceId)
				         .text(device.deviceName));
				popup.countrycode = device.countryCode;
				chrome.extension.sendMessage({status: "logginin"}, function() {});
			}
			reload_popup();

			if (deviceCount == 0) {
				show_no_devices_msg_();
			}
		});
	}
};

popup.request_failed = function(jqXHR, textStatus, e) {
	show_hide_loading_screen_(false);
	util.handle_error("AJAX Error (" + e +")");
};

// Starting point
$(document).ready(function() {
	//Configure Tabs
	$( "#tabs" ).tabs();
	$("#tabs").tabs({disabled: [2,2]});
	$('#send_message').click(send_message_);

	// Show loading screen.
	show_hide_loading_screen_(true);

	// Request user configuration (E.g. Phone no, contacts)
	$.get(environment.userconfig_url)
		.success(popup.request_success)
		.error(popup.request_failed);
	
	// On - key up for phone number
	$('#phonenumber').keyup(popup.event_enteringphonenumber);

	// On click call button.
	$("#callbutton").button().click(function() {
		var note = $("#quicknote").val();
		var number =  $('#txt_formatted_number').html();
		if (!note || note=="") {note = "Call " + number;}
		var url = environment.request_call_url + 'no=' + encodeURIComponent(number) +
			'&device=' + $("#phone_list").val() + '&note=' + encodeURIComponent(note) +
			'&time=' +  new Date().getTime();
		$.get(url)
			.success(function(data) {
					if ("OK" == data) {
						var notification = $("#infomessage");
						util.show_notification(notification, 3000);
					} else {
						var notification = $("#errormessage");
						var errortext = $("#errormessage_p");
						errortext.text(data);
						util.show_notification(notification, 6000);
					}
				})
			.error(function() {
				var notification = $("#errormessage");
				var errortext = $("#errormessage_p");
				errortext.text("Internal server error");
				util.show_notification(notification, 6000);
			});
	});
 });
show_hide_loading_screen_ = function(show) {
	if (show) {
		$( "#dialog-loading" ).dialog({
			  closeOnEscape: false,
		      height: 80,
		      modal: true
		    });
		$(".ui-dialog-titlebar-close").hide();
	} else {
		$( "#dialog-loading" ).dialog('close');
	}
};

show_no_devices_msg_ = function() {
	$( "#dialog-nodevices" ).dialog({
		  closeOnEscape: false,
	      height: 295,
	      modal: true
	    });
	$(".ui-dialog-titlebar-close").hide();
};

reload_popup = function() {
	var conversationList = localStorage.conversations;
	$( "#selectable").html('');
	$( "#selectable")
		.append('<li id="c_start_new" class="ui-widget-content">New Conversation</li>');
	
	if (conversationList) {
		conversationList = JSON.parse(conversationList);

		for (var conv in conversationList.reverse()) {
	
			var c = conversationList[conv];
		
			// If this is not an array we are using old data. Re-set all of them.
			try {
				var messages = JSON.parse(localStorage[c]);
			} catch (e) {
				messages = null;
			}
			
			if (!$.isArray(messages)) {
				localStorage.removeItem(c) ;
				localStorage.removeItem('conversations');
				break;
			}

			// Load first conversation of the list.
			c = messages[0];
			var from = c.no;
			if (c.contactName != '?') {
				from = c.contactName;
			} else {
				from = formatLocal(popup.countrycode, from);
			}
			if (c) {
				$( "#selectable")
					.append('<li id="'+ conversationList[conv] +'" class="ui-widget-content">' + from + '<BR>-' + c.fromPhone + '</li>');
				
			}
		}
	}
	$( "#selectable").selectable();
	$( "#selectable").bind( "selectableselected", show_conversations_);

	if (popup.selectedconversation) {
		$('#selectable')
		.find('li')
		  .removeClass('ui-selected')
		  .end();
		$("#"+popup.selectedconversation)
		  .addClass('ui-selected');
		refresh_conversation_view_(popup.selectedconversation);
	}
};

show_conversations_ = function(event, ui) {
	var conversation = ui.selected.id;
	if (conversation != 'c_start_new') {
		
		$("#message_window").show();
		$('#txtNewMessage').keyup(count_characters_);
		popup.selectedconversation = conversation;
		
		refresh_conversation_view_(conversation);
	} else {
		$("#btnNewConversationOK").click(function(){
			var phonenum = formatE164(popup.countrycode, $('#sms_phonenumber').val());
			var newMessage = new Object();
			newMessage.time = new Date().getTime();
			newMessage.phoneId = $("#phone_list").val();
			newMessage.type = "NULL_SMS";
			newMessage.messageContents = '';
			newMessage.sent_by_me = true;
			newMessage.no = phonenum;
			newMessage.fromPhone = $("#phone_list").text();
			newMessage.contactName = '?';

			popup.selectedconversation = storeConversation(newMessage);
			$("#message_window").show();
			
			$( "#dialog-newconversation" ).dialog( "close" );
		});
		$("#btnNewConversationCancel").click(function(){
			$( "#dialog-newconversation" ).dialog( "close" );
		});
		$('#sms_phonenumber').keyup(function(){
			var formatted = formatLocal(popup.countrycode, $('#sms_phonenumber').val());
			$('#sms_phonenumber').val(formatted);
			var phoneUtil = i18n.phonenumbers.PhoneNumberUtil.getInstance();
			var n = phoneUtil.parseAndKeepRawInput(formatted, popup.countrycode);
			if (phoneUtil.isValidNumber(n, popup.countrycode)) {
				$("#btnNewConversationOK").removeAttr("disabled");
			} else {
				$("#btnNewConversationOK").attr("disabled", "disabled");
			}
		});
		$( "#dialog-newconversation" ).dialog({
		      height: 120,
		      width: 375,
		      modal: true,
		      close: function( event, ui ) {
		    	  reload_popup();
		      }
		    });
	}
};

count_characters_ = function() {
	var charCount = $("#txtNewMessage").val().length;
	var msgCount = Math.round(charCount / 160) + 1;

	$("#character_count").text((charCount%160) + ' / 160 Message '+ msgCount);
};

send_message_ = function() {
	var conversation = JSON.parse(localStorage[popup.selectedconversation]);
	var phoneId = conversation[0].phoneId;
	
	var newMessage = new Object();
	newMessage.time = new Date().getTime();
	newMessage.phoneId = phoneId;
	newMessage.type = "OUTGOING_SMS";
	newMessage.messageContents = $("#txtNewMessage").val();
	newMessage.sent_by_me = true;
	newMessage.no = conversation[0].no;
	newMessage.contactName = '?';
	
	popup.new_message = newMessage;

	$.get(environment.send_sms + "?m=" + encodeURIComponent(JSON.stringify(newMessage)))
		.success(function(data){
			if ("OK" == data) {
				storeConversation(popup.new_message);
				refresh_conversation_view_(popup.selectedconversation);
				$("#txtNewMessage").val('');
			} else {
				var notification = $("#errormessage");
				var errortext = $("#errormessage_p");
				errortext.text(data);
				util.show_notification(notification, 6000);
				$("#txtNewMessage").val(popup.new_message.messageContents);
			}
		})
		.error(function() {
			var notification = $("#errormessage");
			var errortext = $("#errormessage_p");
			errortext.text('Internal Server Error Occured.');
			util.show_notification(notification, 6000);
			$("#txtNewMessage").val(newMessage.messageContents);
		});
};

refresh_conversation_view_ = function(conversation) {
	var messages = JSON.parse(localStorage[conversation]);
	$("#message_view").html('');
	for (var i in messages) {
		
		var msg = messages[i];
		if (msg.type == "NULL_SMS") {
			continue;
		}
		var color = '#fbec88';
		var align = 'left';
		if (msg.sent_by_me) {
			color = "#c5dbec";
			align = 'right';
		}
		var style = 'width: 240px; background-color: ' + color +'; opacity: 0.9; font-size: 0.7em; margin: 4px; float: ' + align+ ';';
		
		$("#message_view").append(
		  '<div style="width:320px; text-align: ' + align +'; padding-top: 4px; padding-right: 4px;">'
			+ '<div class="ui-corner-all" style="' + style + '">' + msg.messageContents + '</div></div>'
		);
		
	}
	$("#message_view").scrollTop($("#message_view")[0].scrollHeight);
};