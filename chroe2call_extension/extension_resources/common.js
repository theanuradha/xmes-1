//Configuration
environment = {};
environment.mode = "PROD"; // Dev or PROD
environment.remote_server = "https://deskphoneapp.appspot.com/";
environment.local_server = "http://192.168.1.6:60000/";
environment.isextension = true;
environment.CURRENT_VERSION = 10;
environment.CONTACT_SUPPORTED_SINCE = 10;

if (environment.mode == "PROD") {
	environment.current = environment.remote_server;
} else {
	environment.current = environment.local_server;
}

environment.userconfig_url = environment.current + "extension/getuserconfig";
environment.request_call_url = environment.current + "extension/requestCall?";
environment.establish_channel = environment.current + "extension/establishchannel";
environment.send_sms = environment.current + "extension/sendSms";
environment.heartbeat = environment.current + "extension/heartBeat";
environment.synccontacts = environment.current + "extension/newcontacts?version=";
util = {};

//Login redirect
util.assertLoggedIn = function(obj) {
	if (obj.messageType == "NOT_LOGGED_IN") {
		show_login_panel_(function() {
			chrome.tabs.create({"url": obj.loginUrl});
			// Tell background page about the pop-up open and connection established.
			chrome.extension.sendMessage({status: "loggingin"}, function() {});
		});
		return false;
	} else {
		return true;
	}
};

util.show_notification = function (notification, timeout) {
	notification.css("left", (225-notification.width()/2) + 30);
	notification.show( 'blind', {}, 500, function() {});
	setTimeout(function() {
		notification.hide( 'blind', {}, 500, function() {});
	}, timeout);
};

//Erorr handling
util.handle_error = function(e) {
	$("#err").append(e);
	$("div").hide();
};

storeConversation = function (conversation) {
	var prefix = "C_";
	var conversationList = localStorage.conversations;
	
	var key = prefix + conversation.phoneId + "_" + conversation.no.substring(1);
	var existing = localStorage[key];
	if (!existing) {
		existing = new  Array();
	} else {
		existing = JSON.parse(existing);
		// If we have a different contact name, update it now.
		if (conversation.contactName != '?') {
			for (var i in existing) {
				existing[i].contactName = conversation.contactName;
			}
		}
	}
	existing.push(conversation);
	
	localStorage.setItem(key, JSON.stringify(existing));

	if(!conversationList) {
		conversationList = new Array();
	} else {
		conversationList = JSON.parse(conversationList);
	}

	if (conversationList.indexOf(key) != -1) {
		conversationList.splice(conversationList.indexOf(key), 1);
	}

	conversationList.push(key);
	localStorage.setItem("conversations", JSON.stringify(conversationList));

	return key;
};

storeMissedCall = function (missedCall) {
	var missedCalls = localStorage.missedCalls;
	if (!missedCalls) {
		missedCalls = new Array();
	} else {
		missedCalls = JSON.parse(missedCalls);
	}

	missedCalls.unshift(missedCall);
	localStorage.missedCalls = JSON.stringify(missedCalls);
};

getMissedCalls = function() {
	var missedCalls = localStorage.missedCalls;
	if (!missedCalls) {
		missedCalls = new Array();
	} else {
		missedCalls = JSON.parse(missedCalls);
	}
	return missedCalls;
};

getRecentMissedCalls = function() {
	var recentcalls = localStorage.recentcalls;

	if (!recentcalls) {
		recentcalls = new Array();
	} else {
		recentcalls = JSON.parse(recentcalls);
	}
	return recentcalls;
};

getRecentMessages = function() {
	var recentmessages = localStorage.recentmessages;

	if (!recentmessages) {
		recentmessages = new Array();
	} else {
		recentmessages = JSON.parse(recentmessages);
	}
	return recentmessages;
};

storerecentevent = function(type, item) {
	var recenmessages = localStorage.recentmessages;
	var recentcalls = localStorage.recentcalls;

	if (!recentcalls) {
		recentcalls = new Array();
	} else {
		recentcalls = JSON.parse(recentcalls);
	}

	if (!recenmessages) {
		recenmessages = new Array();
	} else {
		recenmessages = JSON.parse(recenmessages);
	}

	if (environment.isextension) {
		var new_notifications = recenmessages.length + recentcalls.length + 1;
		chrome.browserAction.setBadgeText({text: new_notifications + ''});
	}

	if (type == 'sms') {
		recenmessages.unshift(item);
		localStorage.recentmessages = JSON.stringify(recenmessages);
	} else {
		recentcalls.unshift(item);
		localStorage.recentcalls = JSON.stringify(recentcalls);
	}
};

cleanupRecentEvents = function() {
	localStorage.removeItem('recentmessages');
	localStorage.removeItem('recentcalls');

	if (environment.isextension) {
		chrome.browserAction.setBadgeText({text: ''});
	}
};

cleanupAllMissedCalls = function() {
	localStorage.removeItem('missedCalls');
};

//****** PRIVATE METHODS ***/
show_login_panel_ = function(onsignin_click) {
	$("#login_userhomeview").hide();
	$("#login_required").show();
	$("#signInwithGoogle").on("click", onsignin_click);
};

fix_phonenumber_spaces = function(num) {
	if (num && num.charAt(0) == ' ') {
		return '+' + num.slice(1);
	} else {
		return num;
	}
};
