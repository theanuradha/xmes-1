channel = {};

channel.instance = null;

channel.Socket = function() {
	this.channelKey = null;
	this.connected = false;
	this.channel = null;
	this.socket = null;
	this.network_down = false;
	this.initiating_connection = false;
	this.loggedIn = false;
	channel.instance = this;
};

channel.Socket.prototype.isConnected = function() {
	return channel.instance.connected;
};

channel.Socket.prototype.isLoggedIn = function() {
	return channel.instance.loggedIn;
};

channel.Socket.prototype.connect = function() {
	if (!channel.instance.connected || !channel.instance.network_down || !channel.instance.initiating_connection) {
		channel.instance.initiating_connection = true;
		console.log('Creating new channel.');
		var url = environment.establish_channel;
		if (channel.instance.channelKey) {
			url += '?PREVIOUS_CHANNEL_ID=' + channel.instance.channelKey;
		}
		$.get(url)
			.success(channel.instance.handle_connect_response_)
			.error(channel.instance.noNetwork_);
	}
};

channel.Socket.prototype.noNetwork_ = function() {
	console.log('Network error. Re-trying in 30 seconds');
	channel.instance.network_down = true;
	setTimeout(function() {
		$.get(environment.userconfig_url)
			.success(function() {
					channel.instance.network_down = false;
					channel.instance.connect();
				})
			.error(function() {
				channel.instance.noNetwork_();
				console.log('Network still down. Re-trying in 30 seconds');
			});
	}, 30000);
};

channel.Socket.prototype.handle_connect_response_ = function(data) {
	if (data && data.messageType == "NOT_LOGGED_IN") {
		console.log('User not logge in yet. Try later.');
		channel.instance.loggedIn = false;
		channel.instance.connected = false;
		channel.instance.initiating_connection = false;
	} else {
		$('#wcs-iframe').remove();
		channel.instance.loggedIn = true;
		channel.instance.channel = new goog.appengine.Channel(data.msg);
		channel.instance.channelKey = data.msg2;
		channel.instance.socket = channel.instance.channel.open();
		
		var channelSocket = channel.instance;

		channel.instance.socket.onopen = function() {
			channelSocket.connected = true;
			channelSocket.initiating_connection = false;
	    	console.log('Channel Opened.');
	    };
	    channel.instance.socket.onmessage = function(incoming) {
	    	var data = JSON.parse(incoming.data);
	    	if (data.type == 'MISSED_CALL') {
	    		onMissedCall(incoming);
		    } else if (data.type == 'INCOMING_SMS') {
		    	onIncomingSms(incoming);
		    }
	    };
	    channel.instance.socket.onerror = function(e) {
	    	// Wait for few seconds to see its really not connected and retry.
	    	console.log('Error occured: ' + e.description);
	    	channelSocket.destroy_();
	    	setTimeout(function() {
	    		if (!channelSocket.connected) {
	    			channelSocket.connect();
	    		}
	    	}, 10000);
	    };
	    channel.instance.socket.onclose = function(e) {
	    	console.log('Server closed the connection.');
	    	channelSocket.destroy_();
	    	setTimeout(function() {
	    		if (!channelSocket.connected) {
	    			channelSocket.connect();
	    		}
	    	}, 5000);
	    };
	}
};

channel.Socket.prototype.destroy_ = function() {
	channel.instance.connected = false;
	channel.instance.socket = null;
	channel.instance.channel = null;
	$('#wcs-iframe').remove();
};



var keepTryingUntilLoggedIn = function(timeout, c) {
	console.log('Checking if channel established.');
	setTimeout(function() {
		if (!channel.instance.isLoggedIn()) {
			console.log('Not logged in yet. Will check back in ' + timeout);
			channel.instance.connect();
			keepTryingUntilLoggedIn(timeout * 1.3);
		}
	}, timeout);
};
$(document).ready(
		function() {
			var c = new channel.Socket();
			chrome.extension.onMessage.addListener(function(request, sender,
					sendResponse) {
				keepTryingUntilLoggedIn(10000, c);
			});

			c.connect(false);
			// Wait 30 seconds and check if the channel is properly established.
			setTimeout(function() {
				if (!c.isConnected()) {
					showNotification(
							'Click on the extension to log in.',
							'Not logged In',
							'icon_large.png');
				}
			}, 30000);
		});

showNotification = function(msg, title, icon) {
	var notification = webkitNotifications.createNotification(
			  icon,
			  title,
			  msg
			);
	notification.show();
};

onMissedCall = function (data) {
	var alertInfo = JSON.parse(data.data);
	var at = new Date(alertInfo.time);
	var from = alertInfo.no;
	if (alertInfo.contactName != '?') {
		from = alertInfo.contactName + '(' + alertInfo.no + ')';
	}
	showNotification(
			'At ' + at.toLocaleTimeString() + ' On your ' + alertInfo.fromPhone,
			'Missed Call from: ' + from, 'icon_large.png');
};

onIncomingSms = function (data) {
	var alertInfo = JSON.parse(data.data);
	var from = alertInfo.no;
	if (alertInfo.contactName != '?') {
		from = alertInfo.contactName + '(' + alertInfo.no + ')';
	}
	storeConversation(alertInfo);
	showNotification(
			'Click on the Deskphone extension to View/reply. -' + alertInfo.fromPhone,
			'New SMS from: ' + from, 'icon_large.png');
};
