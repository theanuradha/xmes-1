<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" 
                                                  prefix="fn" %>

<html>
<head>
<jsp:include page="commonheader.jsp" />
</head>

<body>
	<jsp:include page="topnavigation.jsp" />
	<div>
		<div id="login_required" class="loginpanel" style="margin: auto;">
			<span class="loading_text">Please Sign In with your Google
				account to continue.</span> <a id="signInwithGoogle" href="${loginurl}"><img
				src="/static/img/google.png" /></a>
		</div>
	</div>

</body>
</html>