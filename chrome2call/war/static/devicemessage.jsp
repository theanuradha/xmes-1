<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" 
                                                  prefix="fn" %>

<html>
<head>
<jsp:include page="commonheader.jsp" />

<script type="text/javascript">
	$(document).ready(function() {
		$('#defaulthome').text('${user}');
	});

	device = function(deviceId, action) {
		var parms = 'device=' + encodeURIComponent(deviceId) + '&action=' + encodeURIComponent(action); 
		$.get('/extension/updateDevice?' + parms, function (data) {
			$('#' + deviceId).remove();
			if ($('#devices').children().length == 0) {
				location.reload();
			}
		})
		.fail(function(e) { console.log('Could not complete: ' + e); });
	};
 </script>
</head>

<body>
	<jsp:include page="topnavigation.jsp" />
	<div>
		<div id="login_required" class="loginpanel" style="margin: auto;">
			<c:if test="${fn:length(unauthenticated) == 0}"> 
				<span class="loading_text">
					No Android devices found for
						this account. Install the DeskPhone Android app on your device and refresh this page.
					<br/><br/><br/>
					<a href="https://play.google.com/store/apps/details?id=com.lahiru.xmes"
						class="large_button" id="android" target="_blank" >
						<span class="icon"></span> <em>Download now for</em> Android
					</a>
				</span>
			</c:if>
			<c:if test="${fn:length(unauthenticated) > 0}"> 
				<span class="loading_text">You have device(s) that hasn't been activated yet. Please take a moment activate 
				or remove those devices from your account.
				</span>
				<table style="width: 500px; margin: auto">
					<tbody id="devices">
					<c:forEach var="device" items="${unauthenticated}">
						<tr id="${device.extensionDeviceId}">
							<td>${device.deviceName}</td>
							<td>${device.phoneNumber}</td>
							<td><a href="#" class="button green" onclick="device('${device.extensionDeviceId}', 'act'); return false;">Activate</a></td>
							<td><a href="#" class="button pink" onclick="device('${device.extensionDeviceId}', 'rm'); return false;">Remove</a></td>
						</tr>
					</c:forEach>
					</tbody>
				</table>
			</c:if>
		</div>
	</div>

</body>
</html>