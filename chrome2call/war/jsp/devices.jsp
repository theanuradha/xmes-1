<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" 
                                                  prefix="fn" %>
<jsp:include page="header.jsp" />
		<div style="float: right; margin: 10px">
			<a style=" line-height: 1; display: block; color: #807c79; text-shadow: 0px 2px 1px #fff"
			href="${linkontop}">${linkontoptext}</a>
		</div>
		<div style="text-align: left; margin-left: 30px; width: 540px; margin-top: 30px; margin-bottom: 45px">
			    <c:if test="${devicecount eq 0}">
			    	<div style="color: #ed8335; font-size: 20px;">
			     		You haven't registered any devices yet.
			     	</div>
			     	
			    </c:if>
			    <c:if test="${devicecount ne 0}">
			    	<div style="color: #ed8335; font-size: 20px;">
			     		<table>
			     			<tr>
			     				<th style="color: #ed8335; font-size: 18px; padding-right: 10px; text-align: left;">
			     					Device Name
			     				</th>
			     				<th style="color: #ed8335; font-size: 18px; padding-right: 10px; text-align: left;">
			     					Phone Number
			     				</th>
			     				<th style="color: #ed8335; font-size: 18px; padding-right: 10px; text-align: left;">
			     					Status
			     				</th>
			     				<th style="color: #ed8335; font-size: 18px; padding-right: 10px; text-align: left;">
			     				</th>
			     			</tr>
			     			<c:forEach var="d" items="${user.devices}" varStatus="s">
			     				<c:if test="${d.authenticated eq true}">
			     					<c:set var="status" value="Active"/>
			     					<c:set var="btnlabel" value="De-Activate"/>
			     				</c:if>
			     				<c:if test="${d.authenticated eq false}">
			     					<c:set var="status" value="Not Active"/>
			     					<c:set var="btnlabel" value="Activate"/>
			     				</c:if>
			     				
    							<tr>
    								
    									<input type="hidden" id="device" value="${d.extensionDeviceId}"/>
    									<input type="hidden" id="isActive" value="${not d.authenticated}"/>
	    								<td style="text-align: left;padding-right: 10px;">${d.deviceName }</td>
	    								<td style="text-align: left;padding-right: 10px;">${d.phoneNumber}</td>
	    								<td style="text-align: left;padding-right: 10px;">${status }</td>
	    								<td style="text-align: left;padding-right: 10px;">
	    									<form method="post" action="d">
	    									<input type="submit" value="${btnlabel}"/>
	    									<input type="hidden" id="device" name="device" value="${d.extensionDeviceId}"/>
    										<input type="hidden" id="isActive" name="isActive" value="${not d.authenticated}"/>
    										</form>
    									</td>
    								
    							</tr>
    							
							</c:forEach>
			     		</table>
			     		<br/>
			     		<table>
			<tr>
				<td>
					<img src="../img/chromeStore.png" style="width: 80px; height: 80px" alt="Get it from Chrome Web Store"/>
				</td>
				<td style="position: relative; top: 0px;vertical-align: top;width: 140px; padding-left: 20px">
					<div style="position: relative; top: 0px;">
						<div style="color: #ed8335; font-size: 18px; position: relative; top: 0px;width: 100%">
							Install the Deskphone extension.
						</div>
						<span style="font-family: 'Proxima Nova',arial,sans-serif; color: #3f3f3f; font-size: 13px; padding-bottom: 7px">
							<a href="https://chrome.google.com/webstore/detail/deskphone-beta/fpajdhbogocogdiippmefhkodickeocp?hl=en">Install from Google Chrome Web Store</a>
						</span>
					</div>
				</td>
			</tr>
			<tr style="height:20px"><td></td></tr>
			<tr style="padding-bottom: 40px">
				<td>
					<img src="../img/googleplay.jpg" style="width: 80px; height: 80px" alt="Get it from Chrome Web Store"/>
				</td>
				<td style="position: relative; top: 0px;vertical-align: top; width: 140px; padding-left: 20px">
					<div style="position: relative; top: 0px; width: 400px">
						<div style="color: #ed8335; font-size: 18px; position: relative; top: 0px;">
							Install the Deskphone Android app.
						</div>
						<span style="font-family: 'Proxima Nova',arial,sans-serif; color: #3f3f3f; font-size: 13px; padding-bottom: 7px">
							<a href="http://play.google.com/store/apps/details?id=com.lahiru.xmes">Install from Google Play Store</a>
						</span>
					</div>
				</td>
			</tr>
		</table>
			     	</div>
			    </c:if>
			</div>
			
		</div>
<jsp:include page="footer.jsp" />