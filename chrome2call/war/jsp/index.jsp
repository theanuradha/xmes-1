<jsp:include page="header.jsp" />
		<div style="float: right; margin: 10px">
			<a style=" line-height: 1; display: block; color: #807c79; text-shadow: 0px 2px 1px #fff"
			href="${linkontop}">${linkontoptext}</a>
		</div>
		<div style="text-align: left; margin-left: 30px; width: 540px; margin-top: 30px; margin-bottom: 45px">
			<div style="color: #ed8335; font-size: 20px;">
				What is Deskphone?
			</div>
			<ul>
				<li style="font-family: 'Proxima Nova',arial,sans-serif; color: #3f3f3f; font-size: 13px; padding-bottom: 7px">
				Send / Receive SMS Directly on Your Browser. Real-time SMS alerts.
				</li>
				<li style="font-family: 'Proxima Nova',arial,sans-serif; color: #3f3f3f; font-size: 13px; padding-bottom: 7px">
				Receive missed call alerts on your desktop.
				</li>
				<li style="font-family: 'Proxima Nova',arial,sans-serif; color: #3f3f3f; font-size: 13px; padding-bottom: 7px">
				Send phone numbers from your browser to phone, Your phone will automatically prompt you to start the call.
				</li>
				<li style="font-family: 'Proxima Nova',arial,sans-serif; color: #3f3f3f; font-size: 13px; padding-bottom: 7px">
				Set call reminders.
				</li>
			</ul>
		</div>
		<div style="text-align: left; margin-left: 30px; width: 540px; margin-top: 30px">
		<table>
			<tr>
				<td>
					<img src="../img/chromeStore.png" style="width: 80px; height: 80px" alt="Get it from Chrome Web Store"/>
				</td>
				<td style="position: relative; top: 0px;vertical-align: top;width: 140px; padding-left: 20px">
					<div style="position: relative; top: 0px;">
						<div style="color: #ed8335; font-size: 18px; position: relative; top: 0px;width: 100%">
							Install the Deskphone extension.
						</div>
						<span style="font-family: 'Proxima Nova',arial,sans-serif; color: #3f3f3f; font-size: 13px; padding-bottom: 7px">
							<a href="https://chrome.google.com/webstore/detail/deskphone-beta/fpajdhbogocogdiippmefhkodickeocp?hl=en">Install from Google Chrome Web Store</a>
						</span>
					</div>
				</td>
			</tr>
			<tr style="height:20px"><td></td></tr>
			<tr style="padding-bottom: 40px">
				<td>
					<img src="../img/googleplay.jpg" style="width: 80px; height: 80px" alt="Get it from Chrome Web Store"/>
				</td>
				<td style="position: relative; top: 0px;vertical-align: top; width: 140px; padding-left: 20px">
					<div style="position: relative; top: 0px; width: 400px">
						<div style="color: #ed8335; font-size: 18px; position: relative; top: 0px;">
							Install the Deskphone Android app.
						</div>
						<span style="font-family: 'Proxima Nova',arial,sans-serif; color: #3f3f3f; font-size: 13px; padding-bottom: 7px">
							<a href="http://play.google.com/store/apps/details?id=com.lahiru.xmes">Install from Google Play Store</a>
						</span>
					</div>
				</td>
			</tr>
		</table>
		</div>
<jsp:include page="footer.jsp" />

