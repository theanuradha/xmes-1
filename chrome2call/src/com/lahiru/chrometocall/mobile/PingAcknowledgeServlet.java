package com.lahiru.chrometocall.mobile;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.lahiru.chrometocall.common.json.PingAcknowledgement;
import com.lahiru.chrometocall.service.TalkToPhoneService;
/**
 * Handles a Ping Acknowledgment coming from phone.
 * @author lahiruw
 *
 */
@SuppressWarnings("serial")
public class PingAcknowledgeServlet extends HttpServlet {
	Logger log = Logger.getLogger(PingAcknowledgeServlet.class.getName());

	@Inject Gson gson;
	@Inject Provider<TalkToPhoneService> talkToPhoneServiceProvider;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
	
		String q = req.getParameter("q");
		q = URLDecoder.decode(q, "utf-8");

		PingAcknowledgement ack = gson.fromJson(q, PingAcknowledgement.class);

		talkToPhoneServiceProvider.get().updateDeviceVersion(ack);
		
		resp.setContentType("text/plain");
		resp.getWriter().write("OK");
	}
}
