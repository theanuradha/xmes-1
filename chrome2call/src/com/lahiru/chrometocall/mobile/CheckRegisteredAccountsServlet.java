package com.lahiru.chrometocall.mobile;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.lahiru.chrometocall.service.UserInfoService;

/**
 * Takes a list of google email addresses as parameters and retuns a list of
 * accounts that are already registered with the system.
 * @author lahiruw
 *
 */
@SuppressWarnings("serial")
public class CheckRegisteredAccountsServlet extends HttpServlet {
	@Inject Gson gson;
	@Inject Provider<UserInfoService> userInfoServiceProvider;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		List<String> accountsRegistered = Lists.newArrayList();
		String[] emails = req.getParameterValues("emails");
		boolean activatedOnly = Boolean.parseBoolean(req.getParameter("authenticatedOnly"));
		String deviceId = req.getParameter("deviceId");

		if (emails != null) {
			for (String email : emails) {
				boolean isOk = userInfoServiceProvider.get()
						.isUserRegisteredInTheSystem(email, activatedOnly, deviceId);
				if (isOk) {
					accountsRegistered.add(email);
				}
			}
		}

		resp.setContentType("application/json");;
		resp.getWriter().write(gson.toJson(accountsRegistered));
	}
}
