package com.lahiru.chrometocall.mobile;

import java.io.IOException;
import java.net.URLDecoder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.lahiru.chrometocall.common.DPConstants;
import com.lahiru.chrometocall.common.json.IncomingSmsAlert;
import com.lahiru.chrometocall.service.AppChannelService;
import com.lahiru.chrometocall.service.UserInfoService;
/**
 * Register the device with the server.
 * @author lahiruw
 *
 */
@SuppressWarnings("serial")
public class IncomingSmsAlertServlet extends HttpServlet {
	@Inject Gson gson;
	@Inject Provider<AppChannelService> channelServiceProvider;
	@Inject Provider<UserInfoService> userInfoServiceProvider;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		String q = req.getParameter("q");
		q = URLDecoder.decode(q, "utf-8");

		IncomingSmsAlert smsAlert = gson.fromJson(q, IncomingSmsAlert.class);

		channelServiceProvider.get().alertNewSms(smsAlert);

		resp.setContentType("text/plain");

		if (userInfoServiceProvider.get().isDeviceMarkedForDeletion(smsAlert.getPhoneId())) {
			resp.getWriter().write(DPConstants.RESPONCE_GCM_NOT_REGISTERED);
		} else {
			resp.getWriter().write(DPConstants.RESPONCE_OK);
		}
	}
}
