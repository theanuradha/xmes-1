package com.lahiru.chrometocall.mobile;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.lahiru.chrometocall.common.e.BaseException;
import com.lahiru.chrometocall.common.json.ContactMessage;
import com.lahiru.chrometocall.service.ContactsService;
/**
 * Register the device with the server.
 * @author lahiruw
 *
 */
@SuppressWarnings("serial")
public class ContactSyncServlet extends HttpServlet {
	Logger log = Logger.getLogger(ContactSyncServlet.class.getName());
	@Inject Gson gson;
	@Inject Provider<ContactsService> contactServiceProvider;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		throw new BaseException("Get not supported for " + req.getPathInfo());
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		Type stringArrayType = new TypeToken<ArrayList<String>>() {}.getType();
		List<String> emails = gson.fromJson(req.getParameter("emails"), stringArrayType);

		String deviceId = req.getParameter("deviceId");

		JsonParser parser = new JsonParser();
    JsonArray Jarray = parser.parse(req.getParameter("contactData")).getAsJsonArray();
    List<ContactMessage> contacts = new ArrayList<ContactMessage>();
    for (JsonElement je : Jarray) {
    	contacts.add(gson.fromJson(je, ContactMessage.class));
    }

    contactServiceProvider.get().syncCotacts(emails,deviceId, contacts);
    resp.setContentType("text/plain");
    resp.getWriter().write("OK");
	}
}
