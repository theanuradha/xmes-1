package com.lahiru.chrometocall.web;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.appengine.api.utils.SystemProperty;
import com.lahiru.chrometocall.guice.WebAppliationModule;

/**
 * Intercepts any requests from web and redirect to the login page.
 *  
 * @author lahiruw
 */
public class WebAuthenticationFilter implements Filter {
	private static final Logger log = 
			Logger.getLogger(WebAuthenticationFilter.class.getName());

	public WebAuthenticationFilter() {
	}
	
	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp,
			FilterChain chain) throws IOException, ServletException {
		UserService userService = UserServiceFactory.getUserService();
		User user = userService.getCurrentUser();

		HttpServletResponse response = (HttpServletResponse)resp;
		HttpServletRequest request = (HttpServletRequest) req;

		if (user == null) {
			log.info("Not logged in. Request for: " + request.getRequestURI()
					+ " From: " + request.getRemoteAddr());

			String redirectUri = request.getRequestURI();
			if (redirectUri != WebAppliationModule.WEB_DEVICE_LIST) {
				redirectUri = WebAppliationModule.WEB_INDEX;
			}
			String loginURL = getDomainPrefix(request)
					+ userService.createLoginURL(redirectUri);

			response.sendRedirect(loginURL);
		} else {
			chain.doFilter(req, resp);
		}
	}

	//If local, the hostname + port needs to be prefixed.
	private String getDomainPrefix(HttpServletRequest request) {
		if (SystemProperty.environment.value() == SystemProperty.Environment.Value.Development) {
			return "http://" + request.getLocalName() + ":" + request.getLocalPort();
		} else {
			return "";
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}
}
