package com.lahiru.chrometocall.web;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.lahiru.chrometocall.common.e.BaseException;
import com.lahiru.chrometocall.common.json.UserInfoMessage;
import com.lahiru.chrometocall.guice.WebAppliationModule;
import com.lahiru.chrometocall.service.TalkToPhoneService;
import com.lahiru.chrometocall.service.UserInfoService;
import com.lahiru.chrometocall.servlet.StaticContentHostServlet;

@SuppressWarnings("serial")
public class DeviceServlet extends HttpServlet {
	private final Provider<UserInfoService> userInfoServiceProvider;
	@Inject Provider<TalkToPhoneService> talkToPhoneServiceProvider;

	@Inject
	public DeviceServlet(Provider<UserInfoService> userInfoServiceProvider) {
		this.userInfoServiceProvider = userInfoServiceProvider;
	}

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		String view = StaticContentHostServlet.DESKPHONE_DEVICES;
		try {
			UserInfoMessage userDetails = userInfoServiceProvider.get().retrieveCurrentUserDetails();

			try {
				req.setAttribute("user", userDetails);
				req.setAttribute("devicecount",	 userDetails.getDevices().size());
				req.setAttribute("linkontop", WebRequestUtil.getLogoutUrl(req));
				req.setAttribute("linkontoptext", "Sign Out");

				req.getRequestDispatcher(view).forward(req, resp);
			} catch (ServletException e1) {
				throw new IOException(e1);
			}
		} catch (BaseException e) {
			resp.sendRedirect(WebAppliationModule.WEB_INDEX);
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		boolean isActive = Boolean.parseBoolean(req.getParameter("isActive"));
		String deviceId = req.getParameter("device");
		Logger.getLogger(this.getClass().getName()).warning(deviceId);
		userInfoServiceProvider.get().setDeviceActivationStatus(deviceId, isActive);

		talkToPhoneServiceProvider.get().setAccountStatus(deviceId, isActive);

		resp.sendRedirect(WebAppliationModule.WEB_INDEX);
	}
}
