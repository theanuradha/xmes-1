package com.lahiru.chrometocall.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.lahiru.chrometocall.common.e.BaseException;
import com.lahiru.chrometocall.common.e.NoUserAccountExeption;
import com.lahiru.chrometocall.common.json.UserInfoMessage;
import com.lahiru.chrometocall.guice.WebAppliationModule;
import com.lahiru.chrometocall.service.UserInfoService;
import com.lahiru.chrometocall.servlet.StaticContentHostServlet;

/**
 * Serves Deskphone home page. If the user is not logged in, regular home page is displayed.
 * If the user is signed in, but not registered in the extension, home page is displayed with 
 * sign out link. If this is a valid user with extension or Android registered user is taken to
 * the Device page - a redirect
 * @author lahiruw
 *
 */
@SuppressWarnings("serial")
public class UserHomeServlet extends HttpServlet {
	private final Provider<UserInfoService> userInfoServiceProvider;

	@Inject
	public UserHomeServlet(Provider<UserInfoService> userInfoServiceProvider) {
		this.userInfoServiceProvider = userInfoServiceProvider;
	}

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		String view = "";
		try {
			UserInfoMessage userDetails = userInfoServiceProvider.get().retrieveCurrentUserDetails();

			if (!userDetails.getIsUsingExtension() && userDetails.getDevices().size() == 0) {
				view = handleRegisteredOnlyUser(req);
			} else {
				resp.sendRedirect(WebAppliationModule.WEB_DEVICE_LIST);
			}
		} catch (NoUserAccountExeption e) {
			// This is only thrown when a user is logged in, but no account found in the system db.
			// We need to create a new account with registered in extension set to false.
			userInfoServiceProvider.get().registerIfNew(
					req.getHeader("X-AppEngine-CityLatLong"), false);

			view = handleRegisteredOnlyUser(req);
		} catch (BaseException e) {
			// User not logged in with a Google account.
			req.setAttribute("linkontoptext", "Sign In / Register");
			req.setAttribute("linkontop", WebRequestUtil.getLoginUrl(req));
			view = StaticContentHostServlet.DESKPHONE_HOME_PAGE;
		}

		try {
			req.getRequestDispatcher(view).forward(req, resp);
		} catch (ServletException e1) {
			throw new IOException(e1);
		}
	}

	private String handleRegisteredOnlyUser(HttpServletRequest req) {
		String view;
		String email = userInfoServiceProvider.get().currentUserEmail();
		req.setAttribute("email", email);
		req.setAttribute("linkontop", WebRequestUtil.getLogoutUrl(req));
		req.setAttribute("linkontoptext", "Sign Out");

		view = StaticContentHostServlet.DESKPHONE_HOME_PAGE;
		return view;
	}
}
