package com.lahiru.chrometocall.web.admin;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.TransactionOptions;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.lahiru.chrometocall.service.TalkToPhoneService;
import com.lahiru.chrometocall.service.TalkToPhoneServiceImpl;

@SuppressWarnings("serial")
public class PingAllPhones extends HttpServlet {
	private Logger log = Logger.getLogger(PingAllPhones.class.getName());
	@Inject Provider<TalkToPhoneService> talkToPhoneServiceProvider;

	@SuppressWarnings({ "deprecation", "unchecked" })
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		Transaction transaction = null;
		try {
			log.info("Start pinging all phones.");

			com.google.appengine.api.datastore.DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
			Query q = new Query("ApplicationUser");

			PreparedQuery pq = datastore.prepare(q);

			int deviceCount = 0;
			int disabledDeviceCount = 0;
			for (Entity user : pq.asIterable()) {
				String email = (String) user.getProperty("email");
				Collection<Key> devices = (Collection<Key>) user.getProperty("devices");

				if (devices == null) {
					continue;
				}
				
				for (Key deviceKey : devices) {
					Query dq = new Query("MobileDevice")
						.addFilter(Entity.KEY_RESERVED_PROPERTY,
								Query.FilterOperator.EQUAL,
								deviceKey);
					List<Entity> device = datastore.prepare(dq).asList(FetchOptions.Builder.withLimit(1));
					if (device.size() == 0) {
						continue;
					}

					Entity result = device.get(0);
					String c2dmId = (String) result.getProperty("c2dmRegistrationId");
					boolean isActive = (Boolean) result.getProperty("authenticated");
					boolean isMarkedForDeletion = (Boolean) result.getProperty("isMarkedForDeletion");

					if (isMarkedForDeletion) {
						continue;
					}
	
					transaction = datastore.beginTransaction(TransactionOptions.Builder.withXG(true));
					String sendStatus = talkToPhoneServiceProvider.get().sendPingRequest(c2dmId, isActive, email);
					if (TalkToPhoneServiceImpl.RESPONCE_OK.equals(sendStatus)) {
						log.info("Ping request sent");
						result.setProperty("isMarkedForDeletion", false);
					} else if (TalkToPhoneServiceImpl.RESPONCE_FAIL.equals(sendStatus)) {
						log.warning("Device not registered. Marking as inactive.");
						result.setProperty("isMarkedForDeletion", true);
						disabledDeviceCount++;
					} else {
						log.warning("Canonical ID found: " + sendStatus + " -> Updating new C2DM ID.");
						result.setProperty("isMarkedForDeletion", false);
						result.setProperty("c2dmRegistrationId", sendStatus);
					}
					datastore.put(result);
					transaction.commit();

					deviceCount ++;
					log.info("Ping request sent to: " + result.getProperty("phoneNumber"));
				}
			}

			log.warning("Disabled " + disabledDeviceCount + " devices.");
			log.info("Ping request sent to " + deviceCount + "devices.");
			resp.setStatus(HttpServletResponse.SC_OK);
			resp.getWriter().write("OK");
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			log.severe(e.getMessage());
			resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
		}
	}
}
