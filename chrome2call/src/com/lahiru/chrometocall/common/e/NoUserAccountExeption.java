package com.lahiru.chrometocall.common.e;

/**
 * Thrown when a user account is not found with given criteria.
 * 
 * @author lahiruw
 *
 */
@SuppressWarnings("serial")
public class NoUserAccountExeption extends RuntimeException {

	public NoUserAccountExeption(String errorMessage) {
		super(errorMessage);
	}
}
