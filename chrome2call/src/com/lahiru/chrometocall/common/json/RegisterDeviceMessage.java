package com.lahiru.chrometocall.common.json;

/**
 * Message sent by a mobile device to register itself with the server.
 * @author lahiruw
 *
 */
public class RegisterDeviceMessage {
	private String email;
	private String c2dmHandler;
	private String phoneNumber;
	private String country;
	private String name;
	private String registrationId;
	private boolean authenticated;
	private int dpVersion;

	private RegisterDeviceMessage(Builder builder) {
		this.email = builder.email;
		this.c2dmHandler = builder.c2dmHandler;
		this.phoneNumber = builder.phoneNumber;
		this.country = builder.country;
		this.name = builder.name;
		this.registrationId = builder.registrationId;
		this.authenticated = builder.authenticated;
		this.dpVersion = builder.dpVersion;
	}

	public RegisterDeviceMessage() {}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getC2dmHandler() {
		return c2dmHandler;
	}

	public void setC2dmHandler(String c2dmHandler) {
		this.c2dmHandler = c2dmHandler;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRegistrationId() {
		return registrationId;
	}

	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}

	public static class Builder {
		private String email;
		private String c2dmHandler;
		private String phoneNumber;
		private String country;
		private String name;
		private String registrationId;
		private boolean authenticated;
		private int dpVersion;

		public Builder email(String email) {
			this.email = email;
			return this;
		}

		public Builder authenticated(boolean authenticated) {
			this.authenticated = authenticated;
			return this;
		}

		public Builder c2dmHandler(String c2dmHandler) {
			this.c2dmHandler = c2dmHandler;
			return this;
		}

		public Builder phoneNumber(String phoneNumber) {
			this.phoneNumber = phoneNumber;
			return this;
		}

		public Builder country(String country) {
			this.country = country;
			return this;
		}

		public Builder name(String name) {
			this.name = name;
			return this;
		}

		public Builder registrationId(String registrationId) {
			this.registrationId = registrationId;
			return this;
		}

		public RegisterDeviceMessage build() {
			return new RegisterDeviceMessage(this);
		}

		public Builder setDpVersion(int dpVersion) {
			this.dpVersion = dpVersion;
			return this;
		}
	}

	public Builder builder() {
		return new Builder();
	}

	public boolean isAuthenticated() {
		return authenticated;
	}

	public void setAuthenticated(boolean authenticated) {
		this.authenticated = authenticated;
	}

	public int getDpVersion() {
		return dpVersion;
	}

	public void setDpVersion(int dpVersion) {
		this.dpVersion = dpVersion;
	}
}
