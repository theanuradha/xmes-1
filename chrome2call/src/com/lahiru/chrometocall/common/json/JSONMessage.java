package com.lahiru.chrometocall.common.json;

/**
 * Every JSON message must extend from this class.
 * @author lahiruw
 *
 */
public class JSONMessage {
	private MessageType messageType;
	private String msg;
	private String msg2;

	protected JSONMessage(MessageType messageType) {
		this.messageType = messageType;
	}

	/**
	 * Builds a default general purpose text message that could contain upto 2 parameters.
	 */
	public JSONMessage(String msg, String msg2) {
		this.messageType = MessageType.TEXT_MESSAGE;
		this.msg = msg;
		this.msg2 = msg2;
	}

	public enum MessageType {
		NOT_LOGGED_IN,
		CURRENT_USER_INFO,
		DEVICE_INFO_MESSAGE,
		TEXT_MESSAGE
	}

	public MessageType getMessageType() {
		return messageType;
	}

	public void setMessageType(MessageType messageType) {
		this.messageType = messageType;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getMsg2() {
		return msg2;
	}

	public void setMsg2(String msg2) {
		this.msg2 = msg2;
	}
}

