package com.lahiru.chrometocall.common.json;

public class MissedCallAlert extends PhoneAlert {
	private String no;
	private String contactName;
	private long contactId;

	public MissedCallAlert() {
		setType("MISSED_CALL");
	}

	public String getNo() {
		return no;
	}

	public String getContactName() {
		return contactName;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public long getContactId() {
		return contactId;
	}

	public void setContactId(long contactId) {
		this.contactId = contactId;
	}
}