package com.lahiru.chrometocall.common.json;

public class PingAcknowledgement {
	private String phoneId;
	private String dpVersion;
	private String email;

	public String getDpVersion() {
		return dpVersion;
	}
	public void setDpVersion(String dpVersion) {
		this.dpVersion = dpVersion;
	}
	public String getPhoneId() {
		return phoneId;
	}
	public void setPhoneId(String phoneId) {
		this.phoneId = phoneId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
}
