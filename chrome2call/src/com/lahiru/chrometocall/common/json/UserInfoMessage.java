package com.lahiru.chrometocall.common.json;

import java.util.Collection;
import java.util.List;

import com.google.common.collect.Lists;

/**
 * JSON Message containing User Info/Configuration.
 * @author lahiru
 */
public class UserInfoMessage extends JSONMessage {

	protected UserInfoMessage() {
		super(MessageType.CURRENT_USER_INFO);
	}

	private String userId;
	private List<DeviceInfoMessage> devices;
	private boolean isUsingExtension;
	private long contactSetVersion;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public List<DeviceInfoMessage> getDevices() {
		return devices;
	}

	public void setDevices(List<DeviceInfoMessage> devices) {
		this.devices = devices;
	}

	public static UserInfoMessageBuilder builder() {
		return new UserInfoMessageBuilder();
	}

	public boolean getIsUsingExtension() {
		return isUsingExtension;
	}

	public void setIsUsingExtension(boolean isUsingExtension) {
		this.isUsingExtension = isUsingExtension;
	}

	public long getContactSetVersion() {
		return contactSetVersion;
	}

	public void setContactSetVersion(long contactSetVersion) {
		this.contactSetVersion = contactSetVersion;
	}

	public static class UserInfoMessageBuilder {
		private String userId;
		private List<DeviceInfoMessage> devices = Lists.newArrayList();
		private boolean isUsingExtension;
		private long contactSetVersion;

		public UserInfoMessageBuilder setUserId(String userId) {
			this.userId = userId;
			return this;
		}

		public UserInfoMessageBuilder addDevices(Collection<? extends DeviceInfoMessage> devices) {
			this.devices.addAll(devices);
			return this;
		}

		public UserInfoMessageBuilder setContactSetVersion(long version) {
			this.contactSetVersion = version;
			return this;
		}

		public UserInfoMessage build() {
			UserInfoMessage userInfoMessage = new UserInfoMessage();
			userInfoMessage.setUserId(userId);
			userInfoMessage.setDevices(devices);
			userInfoMessage.setIsUsingExtension(isUsingExtension);
			userInfoMessage.setContactSetVersion(contactSetVersion);

			return userInfoMessage;
		}

		public UserInfoMessageBuilder setUsingExtension(boolean isUsingExtension) {
			this.isUsingExtension = isUsingExtension;
			return this;
		}
	}
}
