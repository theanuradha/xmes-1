package com.lahiru.chrometocall.entitiy;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.google.appengine.api.datastore.Key;

@SuppressWarnings("serial")
@Entity
public class ChannelSession implements Serializable {
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Key sessionId;
	private String channelId;
	private String originatedIp;
	private Date creationDate;
	private String user;
	private String location;

	public static ChannelSession.Builder builder() {
		return new ChannelSession.Builder();
	}
	
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getOriginatedIp() {
		return originatedIp;
	}

	public void setOriginatedIp(String originatedIp) {
		this.originatedIp = originatedIp;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public static class Builder {
		private String channelId;
		private String originatedIp;
		private Date creationDate;
		private String user;
		private String location;

		public Builder channelId(String channelId) {
			this.channelId = channelId;
			return this;
		}

		public Builder originatedIp(String originatedIp) {
			this.originatedIp = originatedIp;
			return this;
		}

		public Builder user(String user) {
			this.user = user;
			return this;
		}

		public Builder creationDate(Date creationDate) {
			this.creationDate = creationDate;
			return this;
		}

		public ChannelSession build() {
			ChannelSession channelSession = new ChannelSession();
			channelSession.channelId = channelId;
			channelSession.originatedIp = originatedIp;
			channelSession.creationDate = creationDate;
			channelSession.user = user;
			channelSession.location = location;
			return channelSession;
		}

		public Builder setLocation(String location) {
			this.location = location;
			return this;
		}
	}

	@Override
	public int hashCode() {
		return channelId.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ChannelSession) {
			return ((ChannelSession) obj).channelId == this.channelId;
		} else {
			return false;
		}
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public Key getSessionId() {
		return sessionId;
	}

	public void setSessionId(Key sessionId) {
		this.sessionId = sessionId;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
}
