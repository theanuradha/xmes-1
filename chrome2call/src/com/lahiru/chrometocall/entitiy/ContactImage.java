package com.lahiru.chrometocall.entitiy;

import java.io.Serializable;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.annotation.Unindexed;

@Entity
public class ContactImage implements Serializable {

	private static final long serialVersionUID = -5903590033977474038L;

	@Id Long id;
	@Unindexed byte[] image;
	@Parent
	private Key<Person> parent;
	
	public ContactImage() {}

	public ContactImage(Long id, byte[] image, Key<Person> parent) {
		this.id = id;
		this.image = image;
		this.parent = parent;
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public byte[] getImage() {
		return image;
	}
	
	public void setImage(byte[] image) {
		this.image = image;
	}

	public Key<Person> getParent() {
		return parent;
	}

	public void setParent(Key<Person> parent) {
		this.parent = parent;
	}
}
