package com.lahiru.chrometocall.entitiy;

import java.io.Serializable;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Parent;

public class SMS implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	Long id;
	@Parent
	Key<Conversation> conversation;
	String message;
	Long time;
	String status;
	String type;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Key<Conversation> getConversation() {
		return conversation;
	}

	public void setConversation(Key<Conversation> conversation) {
		this.conversation = conversation;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public static class Builder {
		private Long id;
		private Key<Conversation> conversation;
		private String message;
		private Long time;
		private String status;
		private String type;

		public Builder id(Long id) {
			this.id = id;
			return this;
		}

		public Builder conversation(Key<Conversation> conversation) {
			this.conversation = conversation;
			return this;
		}

		public Builder message(String message) {
			this.message = message;
			return this;
		}

		public Builder type(String type) {
			this.type = type;
			return this;
		}

		public Builder time(Long time) {
			this.time = time;
			return this;
		}

		public Builder status(String status) {
			this.status = status;
			return this;
		}

		public SMS build() {
			SMS sMS = new SMS();
			sMS.id = id;
			sMS.conversation = conversation;
			sMS.message = message;
			sMS.time = time;
			sMS.status = status;
			sMS.type = type;
			return sMS;
		}
	}
}
