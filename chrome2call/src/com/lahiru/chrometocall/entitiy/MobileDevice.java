package com.lahiru.chrometocall.entitiy;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.google.appengine.api.datastore.Key;

/**
 * Represents a mobile device which a valid user has registered.
 * 
 * @author lahiruw
 *
 */
@SuppressWarnings("serial")
@Entity
public class MobileDevice implements Serializable {
	// An ID used by the system to uniquely identify a device.
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Key deviceId;
	//Should be of the format +<country code XX>-<Number> if international.
	// If US +1<area code>-<XXX>-<XXXX>
	private String phoneNumber;
	// Phone's C2DM registration key.
	private String c2dmRegistrationId;
	// Phone or tablet.
	private boolean isPhone;
	// Country where the phone is currently being used.
	private String country;
	// Device Name Model (Phone Number) E.g. Samsung Galaxy (+1800-111-2345)
	private String name;
	// Unique Device ID.
	private String registrationId;
	// Has been authenticated by the user.
	private boolean authenticated;
	// Current version
	private int dpVersion;
	// Last heard from this phone.
	private Date lastHeardOn;
	// Marked for deletion
	private boolean isMarkedForDeletion = false;

	public Key getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(Key deviceId) {
		this.deviceId = deviceId;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getC2dmRegistrationId() {
		return c2dmRegistrationId;
	}

	public void setC2dmRegistrationId(String c2dmRegistrationId) {
		this.c2dmRegistrationId = c2dmRegistrationId;
	}

	public boolean isPhone() {
		return isPhone;
	}

	public void setPhone(boolean isPhone) {
		this.isPhone = isPhone;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRegistrationId() {
		return registrationId;
	}

	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}

	public static class MobileDeviceBuilder {
		private String phoneNumber;
		private String c2dmRegistrationId;
		private boolean isPhone;
		private String country;
		private String name;
		private String registrationId;
		private boolean authenticated;
		private int dpVersion;
		private Date lastHeardOn;
		private boolean isMarkedForDeletion;

		public MobileDeviceBuilder setPhoneNumber(String phoneNumber) {
			this.phoneNumber = phoneNumber;
			return this;
		}

		public MobileDeviceBuilder setAuthenticated(boolean authenticated) {
			this.authenticated = authenticated;
			return this;
		}

		public MobileDeviceBuilder setC2dmRegistrationId(String c2dmRegistrationId) {
			this.c2dmRegistrationId = c2dmRegistrationId;
			return this;
		}

		public MobileDeviceBuilder setPhone(boolean isPhone) {
			this.isPhone = isPhone;
			return this;
		}

		public MobileDeviceBuilder setCountry(String country) {
			this.country = country;
			return this;
		}

		public MobileDeviceBuilder setName(String name) {
			this.name = name;
			return this;
		}

		public MobileDeviceBuilder setRegistrationId(String registrationId) {
			this.registrationId = registrationId;
			return this;
		}

		public MobileDeviceBuilder setVersion(int version) {
			this.dpVersion = version;
			return this;
		}

		public MobileDeviceBuilder setLastHeardOn(Date lastHeardOn) {
			this.lastHeardOn = lastHeardOn;
			return this;
		}

		public MobileDeviceBuilder setMarkedForDeletion(boolean isMarkedForDeletion) {
			this.isMarkedForDeletion = isMarkedForDeletion;
			return this;
		}

		public MobileDevice build() {
			MobileDevice device = new MobileDevice();
			device.setC2dmRegistrationId(c2dmRegistrationId);
			device.setCountry(country);
			device.setPhone(isPhone);
			device.setPhoneNumber(phoneNumber);
			device.setName(name);
			device.setRegistrationId(registrationId);
			device.setAuthenticated(authenticated);
			device.setLastHeardOn(lastHeardOn);
			device.setDpVersion(dpVersion);
			device.setMarkedForDeletion(isMarkedForDeletion);
			return device;
		}
	}

	public static MobileDeviceBuilder builder() {
		return new MobileDeviceBuilder();
	}

	public boolean isAuthenticated() {
		return authenticated;
	}

	public void setAuthenticated(boolean authenticated) {
		this.authenticated = authenticated;
	}

	public int getDpVersion() {
		return dpVersion;
	}

	public void setDpVersion(int version) {
		this.dpVersion = version;
	}

	public Date getLastHeardOn() {
		return lastHeardOn;
	}

	public void setLastHeardOn(Date lastHeardOn) {
		this.lastHeardOn = lastHeardOn;
	}

	public boolean isMarkedForDeletion() {
		return isMarkedForDeletion;
	}

	public void setMarkedForDeletion(boolean isMarkedForDeletion) {
		this.isMarkedForDeletion = isMarkedForDeletion;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((registrationId == null) ? 0 : registrationId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MobileDevice other = (MobileDevice) obj;
		if (registrationId == null) {
			if (other.registrationId != null)
				return false;
		} else if (!registrationId.equals(other.registrationId))
			return false;
		return true;
	}

	
}
