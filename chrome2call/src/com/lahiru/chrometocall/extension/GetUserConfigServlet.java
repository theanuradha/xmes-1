package com.lahiru.chrometocall.extension;
import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.lahiru.chrometocall.common.e.NoUserAccountExeption;
import com.lahiru.chrometocall.common.json.UserInfoMessage;
import com.lahiru.chrometocall.service.UserInfoService;
import com.lahiru.chrometocall.web.WebRequestUtil;

/**
 * Returns entire set of user configurations. Such as registered devices Etc.
 * @author Lahiru
 */
@SuppressWarnings("serial")
public class GetUserConfigServlet extends HttpServlet {

	private final Gson gson;
	private final Provider<UserInfoService> userInfoServiceProvider;

	@Inject
	public GetUserConfigServlet(Gson gson, Provider<UserInfoService> userInfoServiceProvider) {
		this.gson = gson;
		this.userInfoServiceProvider = userInfoServiceProvider;
	}

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		UserInfoMessage userDetails;
		try {
			userDetails = userInfoServiceProvider.get().retrieveCurrentUserDetails(true);
			resp.setContentType("application/json");
			resp.getWriter().write(gson.toJson(userDetails));
		} catch (NoUserAccountExeption e) {
			String logoutUrl = WebRequestUtil.getLogoutUrl(req);
			resp.sendRedirect(logoutUrl);
		}
	}
}
