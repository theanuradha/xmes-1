package com.lahiru.chrometocall.extension;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.appengine.api.utils.SystemProperty;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.lahiru.chrometocall.common.json.NotLoggedInMessage;
import com.lahiru.chrometocall.guice.WebAppliationModule;

/**
 * Intercepts any requests from extension and send a NOT_LOGGED_IN
 * Response if the user is not logged into the application.
 *  
 * @author lahiruw
 */
public class ExtensionAuthenticationFilter implements Filter {
	private static final Logger log = 
			Logger.getLogger(ExtensionAuthenticationFilter.class.getName());
	@Inject private Gson gson;
	
	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp,
			FilterChain chain) throws IOException, ServletException {
		UserService userService = UserServiceFactory.getUserService();
		User user = userService.getCurrentUser();

		HttpServletResponse response = (HttpServletResponse)resp;
		HttpServletRequest request = (HttpServletRequest) req;

		if (user == null) {
			log.info("Not logged in. Request for: " + request.getRequestURI()
					+ " From: " + request.getRemoteAddr());

			String loginURL = getDomainPrefix(request)
					+ userService.createLoginURL(redirectUrl());

			sendResponse(req, response, loginURL);
		} else {
			chain.doFilter(req, resp);
		}
	}

	protected String redirectUrl() {
		return WebAppliationModule.HANDLE_NEW_USER;
	}

	protected void sendResponse(ServletRequest req, HttpServletResponse response, String loginURL)
			throws MalformedURLException, IOException, ServletException {
		NotLoggedInMessage message = NotLoggedInMessage.builder()
				.setLoginUrl(new URL(loginURL))
				.build();
		String json = getGson().toJson(message);

		response.setContentType("application/json");
		response.getWriter().write(json);
	}

	//If local, the hostname + port needs to be prefixed.
	private String getDomainPrefix(HttpServletRequest request) {
		if (SystemProperty.environment.value() == SystemProperty.Environment.Value.Development) {
			return "http://" + request.getLocalName() + ":" + request.getLocalPort();
		} else {
			return "";
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}

	public Gson getGson() {
		return gson;
	}

	public void setGson(Gson gson) {
		this.gson = gson;
	}
}
