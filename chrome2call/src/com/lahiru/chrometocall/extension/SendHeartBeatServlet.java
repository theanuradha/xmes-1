package com.lahiru.chrometocall.extension;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.lahiru.chrometocall.service.AppChannelService;

/**
 * Handles make a call request coming from an extension.
 * @author lahiruw
 *
 */
@SuppressWarnings("serial")
public class SendHeartBeatServlet extends HttpServlet {

	private static final String PREVIOUS_CHANNEL_ID = "PREVIOUS_CHANNEL_ID";
	@Inject Provider<AppChannelService> channelServiceProvider;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		String previousChannelId = req.getParameter(PREVIOUS_CHANNEL_ID);
		channelServiceProvider.get().sendKeepAlive(previousChannelId);

		resp.setContentType("application/text");
		resp.getWriter().write("OK");
	}

}
