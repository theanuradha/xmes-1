package com.lahiru.chrometocall.extension;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.lahiru.chrometocall.service.TalkToPhoneService;
import com.lahiru.chrometocall.service.TalkToPhoneService.CallRequest;

/**
 * Handles make a call request coming from an extension.
 * @author lahiruw
 *
 */
@SuppressWarnings("serial")
public class CallRequestServlet extends HttpServlet {

	private static final String NOTE = "note";
	private static final String TIME = "time";
	private static final String PHONE_NUMBER = "no";
	private static final String DEVICE_ID = "device";

	@Inject Provider<TalkToPhoneService> talkToPhoneServiceProvider;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		String device = req.getParameter(DEVICE_ID);
		String requestedPhoneNumber = req.getParameter(PHONE_NUMBER);
		String requestedOn = req.getParameter(TIME);
		String note = req.getParameter(NOTE);

		String status = talkToPhoneServiceProvider.get().placeCall(
				device,
				new CallRequest(requestedPhoneNumber, note, requestedOn));

		resp.getWriter().write(status);
	}

}
