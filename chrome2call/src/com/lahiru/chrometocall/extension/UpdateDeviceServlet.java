package com.lahiru.chrometocall.extension;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.lahiru.chrometocall.common.e.BaseException;
import com.lahiru.chrometocall.service.TalkToPhoneService;
import com.lahiru.chrometocall.service.UserInfoService;

/**
 * Update device servlet.
 * 
 * @author lahiruw
 *
 */
@SuppressWarnings("serial")
public class UpdateDeviceServlet extends HttpServlet {
	private final Provider<UserInfoService> userInfoServiceProvider;
	@Inject private Provider<TalkToPhoneService> talkToPhoneServiceProvider;

	@Inject
	public UpdateDeviceServlet(Provider<UserInfoService> userInfoServiceProvider) {
		this.userInfoServiceProvider = userInfoServiceProvider;
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String deviceId = req.getParameter("device");
		String action = req.getParameter("action");

		if ("rm".equals(action)) {
			userInfoServiceProvider.get().setDeviceMarkedForDeletion(deviceId);
		} else if ("act".equals(action)){
			userInfoServiceProvider.get().setDeviceActivationStatus(deviceId, true);
			talkToPhoneServiceProvider.get().setAccountStatus(deviceId, true);
		} else {
			throw new BaseException("Operation " + action + " not supported.");
		}

		resp.setStatus(200);
	}

}
