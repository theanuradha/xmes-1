package com.lahiru.chrometocall.extension.app;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.lahiru.chrometocall.guice.WebAppliationModule;
import com.lahiru.chrometocall.service.UserInfoService;

@SuppressWarnings("serial")
public class AppLoginServlet extends HttpServlet {
	@Inject private Provider<UserInfoService> userInfoServiceProvider;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String location = req.getHeader("X-AppEngine-CityLatLong");
		userInfoServiceProvider.get().registerIfNew(location, false);

		String redirectUrl = WebAppliationModule.APP_ROOT;
		Cookie[] cookies = req.getCookies();
		for (Cookie cookie : cookies) {
			if ("target".equals(cookie.getName())) {
				redirectUrl = cookie.getValue();
				break;
			}
		}
		resp.sendRedirect(redirectUrl);
	}

}
