package com.lahiru.chrometocall.service;

import java.util.Properties;
import java.util.logging.Logger;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EmailServiceImpl implements EmailService {
	Logger log = Logger.getLogger(EmailServiceImpl.class.getName());

	private String NEW_DEVICE_REGISTERED_EMAIL_BODY = "Welcome,\n\nAnd thank you for installing Deskphone for Android"
			+ " - Your phone away from phone.\n\nTo start sending SMS from your computer and receive real-time alerts,"
			+ " please install the Deskphone Chrome extension by visiting the Google Chrome Webstore. You can also follow the linke below:"
			+ "\n\nhttp://goo.gl/VaBFw \n\n"
			+ "Don't forget to log-in with the same email account (%s) that was used to log in to the Android App."
			+ "\n\nEnjoy!\nDeskphone Team";
	private String NEW_DEVICE_REGISTERED_EMAIL_SUBJECT = "Welcome to Deskphone";

	public void sendNewMobileDeviceRegistrationEmail(String email) {
		try {
			Message msg = new MimeMessage(Session.getDefaultInstance(new Properties()));
			msg.setFrom(new InternetAddress("info@deskphone.mobi", "Deskphone Team"));
			msg.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
			msg.setSubject(NEW_DEVICE_REGISTERED_EMAIL_SUBJECT);
			msg.setText(String.format(NEW_DEVICE_REGISTERED_EMAIL_BODY, email));

			Transport.send(msg);
		} catch(Exception ex) {
			log.warning("Could not send the welcome email" + ex);
		}
	}

}
