package com.lahiru.chrometocall.service;

import java.util.List;

import javax.annotation.Nullable;
import javax.inject.Inject;

import org.apache.commons.codec.binary.Base64;

import com.google.appengine.api.users.User;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.googlecode.objectify.Key;
import com.lahiru.chrometocall.common.e.BaseException;
import com.lahiru.chrometocall.common.json.ContactMessage;
import com.lahiru.chrometocall.common.json.ContactMessage.ContactInfoMessage;
import com.lahiru.chrometocall.common.json.ContactMessageToClient;
import com.lahiru.chrometocall.entitiy.ApplicationUser;
import com.lahiru.chrometocall.entitiy.Contact;
import com.lahiru.chrometocall.entitiy.ContactImage;
import com.lahiru.chrometocall.entitiy.Person;
import com.lahiru.chrometocall.service.da.ApplicationUserManager;
import com.lahiru.chrometocall.service.da.ContactsManager;
import com.lahiru.chrometocall.service.da.PersonManager;

/**
 * {@link ContactsService} implementation.
 * 
 * @author lahiruw
 *
 */
public class ContactServiceImpl implements ContactsService {
	@Inject PersonManager personManager;
	@Inject ApplicationUserManager legacyUserManager;
	@Inject ContactsManager contactsManager;
	@Inject @Nullable User currentUser;

	@Override
	public void syncCotacts(List<String> userEmails, String deviceId, List<ContactMessage> contactDetails) {
		// TODO (lahiru) We are migrating from ApplicationUser object to Person object. Hence,
		// we need to make sure that there is a corresponding Person object exists for every new user
		// created in the system from now on. But once the migration is complete, the check here for
		// existing Person object should go away.

		for (String emailOriginal : userEmails) {
			String emailLowered = emailOriginal.toLowerCase();
			ApplicationUser user = legacyUserManager.lookupUserByEmailAddress(emailLowered);
			if (user == null) {
				throw new BaseException("User " + emailLowered + " not found.");
			}

			Person person = personManager.getPerson(emailLowered);
			Key<Person> personKey;

			// If we are updating a contact, then the current contact set version has to be incremented.
			long newContactSetVersion = 1L;
			if (person == null) {
				person = new Person(emailLowered);
				person.setContactSetVersion(newContactSetVersion);
				personKey = personManager.savePerson(person);
			} else {
				personKey = new Key<Person>(Person.class, person.getEmail());
				newContactSetVersion = person.getContactSetVersion() + 1;
				if (contactDetails != null && contactDetails.size() > 0) {
					person.setContactSetVersion(newContactSetVersion);
					personManager.savePerson(person);
				}
			}

			for (ContactMessage cm : contactDetails) {
				Key<ContactImage> pic = null;

				if (cm.getPicture() != null && cm.getPicture().length() > 0) {
					ContactImage contactImage = new ContactImage(cm.getId(),
							Base64.decodeBase64(cm.getPicture()), personKey);
					pic = contactsManager.storeContactPicture(contactImage);
				}

				List<Contact> contacts = Lists.newArrayList();
				for (ContactInfoMessage im : cm.getContactInfo()) {
					Contact newContact = new Contact.Builder()
							.contactGroupId(cm.getId())
							.id(im.getId())
							.imgage(pic)
							.name(cm.getName())
							.person(personKey)
							.phoneId(deviceId)
							.phoneNumber(im.getPhoneNumber())
							.type(im.getType())
							.version(newContactSetVersion)
							.build();
					contacts.add(newContact);
				}
				contactsManager.storeContacts(contacts);
			}
		}
	}

	@Override
	public byte[] getContactImage(long contactID) {
		return contactsManager.getContactPicture(currentUser.getEmail() ,contactID);
	}

	@Override
	public List<ContactMessageToClient> getContactsAfterVersion(String email,
			long version) {
		Key<Person> person = personManager.createKey(email);
		List<Contact> contacts = null;

		contacts = contactsManager.getContactsAfter(person, version);

		return Lists.transform(contacts, new Function<Contact, ContactMessageToClient>() {
			@Override
			public ContactMessageToClient apply(Contact contact) {
				return new ContactMessageToClient.Builder()
						.id(contact.getId())
						.name(contact.getName())
						.phoneNumber(contact.getPhoneNumber())
						.type(contact.getType())
						.build();
			}});
	}
}
