package com.lahiru.chrometocall.service;

import com.lahiru.chrometocall.common.json.IncomingSmsAlert;
import com.lahiru.chrometocall.common.json.MissedCallAlert;


/**
 * Manages channel connections with the desktop clients.
 * @author lahiruw
 *
 */
public interface AppChannelService {
	/**
	 * Initiate a new channel for the current user.
	 * @param previousChannelId 
	 * @param location 
	 * @return
	 */
	String[] createChannel(String requesterIp, String previousChannelId, String location);

	/**
	 * AAlert user's desktop for missed calls on their phones.
	 * @param alert
	 */
	void alertMissedCall(MissedCallAlert alert);

	/**
	 * AAlert user's desktop for missed calls on their phones.
	 * @param alert
	 */
	void alertNewSms(IncomingSmsAlert alert);

	/**
	 * Sends a keep-alive request to an extension.
	 * @param channelId
	 */
	void sendKeepAlive(String channelId);

	/**
	 * Remove disconnected session.
	 * @param clientId
	 * @throws Exception 
	 */
	void removeUserSession(String previousChannelId) throws Exception;
}
