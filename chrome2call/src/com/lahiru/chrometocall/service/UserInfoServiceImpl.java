package com.lahiru.chrometocall.service;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import javax.annotation.Nullable;

import com.google.appengine.api.users.User;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.servlet.RequestScoped;
import com.lahiru.chrometocall.common.e.BaseException;
import com.lahiru.chrometocall.common.e.NoUserAccountExeption;
import com.lahiru.chrometocall.common.json.DeviceInfoMessage;
import com.lahiru.chrometocall.common.json.RegisterDeviceMessage;
import com.lahiru.chrometocall.common.json.UserInfoMessage;
import com.lahiru.chrometocall.entitiy.ApplicationUser;
import com.lahiru.chrometocall.entitiy.MobileDevice;
import com.lahiru.chrometocall.entitiy.Person;
import com.lahiru.chrometocall.service.da.ApplicationUserManager;
import com.lahiru.chrometocall.service.da.PersonManager;

@RequestScoped
public class UserInfoServiceImpl implements UserInfoService {
	@Inject @Nullable User currentUser;
	@Inject ApplicationUserManager userManager;
	@Inject EmailService emailService;
	@Inject PersonManager personManager;

	private static final Logger log = Logger.getLogger(UserInfoServiceImpl.class.getName());

	@Override
	public String currentUserEmail() {
		return currentUser.getEmail();
	}


	private ApplicationUser getApplicationUser() {
		ApplicationUser applicationUser = userManager.lookupUserByEmailAddress(currentUser.getEmail());

		if (applicationUser == null) {
			throw new NoUserAccountExeption("User " + currentUser.getUserId() + " not found.");
		}
		return applicationUser;
	}

	@Override
	public UserInfoMessage retrieveCurrentUserDetails() {
		return retrieveCurrentUserDetails(false);
	}

	public UserInfoMessage retrieveCurrentUserDetails(boolean isExtension) {
		assertSessionExists();

		ApplicationUser applicationUser = getApplicationUser();

		if (isExtension && !applicationUser.isRegisteredInExtension()) {
			applicationUser.setRegisteredInExtension(true);
			userManager.saveUser(applicationUser);
		}
		Person person = personManager.getPerson(currentUserEmail());

		UserInfoMessage message = convertToMessage(applicationUser);
		message.setContactSetVersion(person != null ? person.getContactSetVersion() : -1);
		return message;
	}

	@Override
	public void registerIfNew(String userLocation, boolean isExtension) {
		assertSessionExists();

		ApplicationUser applicationUser = userManager.lookupUserByEmailAddress(currentUser.getEmail());
		if (applicationUser == null) {
			log.info("Creating new account for " + currentUser.getEmail());

			ApplicationUser newUser = ApplicationUser.builder()
					.setEmail(currentUser.getEmail())
					.setRegisteredDate(new Date())
					.setRegisteredLocation(userLocation)
					.setUserId(currentUser.getUserId())
					.setRegisteredInExtension(isExtension)
					.build();

			try {
				userManager.saveUser(newUser);
			} catch (Exception e) {
				throw new BaseException("Error saving user account.", e);
			}
		} else if (!applicationUser.isRegisteredInExtension()) {
			try {
				// Register previously registered user as an extension user.
				applicationUser.setUserId(currentUser.getUserId());
				applicationUser.setRegisteredInExtension(true);
				userManager.saveUser(applicationUser);
			} catch (Exception e) {
				throw new BaseException("Error saving user account.", e);
			}
		}
	}

	@Override
	public boolean isUserRegisteredInTheSystem(
			String emailAddress, boolean activatedOnly, @Nullable String deviceId) {
		ApplicationUser user = userManager.lookupUserByEmailAddress(emailAddress);

		if (user == null || user.getDevices() == null) {
			return false;
		}

		for (MobileDevice device : user.getDevices()) {
			if (device.getRegistrationId().equals(deviceId)
					&& !device.isMarkedForDeletion()) {
				if (activatedOnly) {
					return device.isAuthenticated();
				} else {
					return true;
				}
			}
		}

		return false;
	}

	/*
	 *  Package default so that other services can utilize this method.
	 */
	static UserInfoMessage convertToMessage(ApplicationUser applicationUser) {
		List<DeviceInfoMessage> devices = Lists.newArrayList();

		if (applicationUser.getDevices() != null) {
			for (MobileDevice device : applicationUser.getDevices()) {
				if (device.isMarkedForDeletion()) {
					// We do not want to sent devices marked for deletion.
					continue;
				}

				devices.add(DeviceInfoMessage.builder()
					.setDeviceName(device.getName())
					.setExtensionDeviceId(device.getRegistrationId())
					.setCountryCode(device.getCountry())
					.setAuthenticated(device.isAuthenticated())
					.setPhoneNumber(device.getPhoneNumber())
					.setVersion(device.getDpVersion())
					.build());
			}
		}

		UserInfoMessage infoMessage = UserInfoMessage.builder()
				.setUserId(applicationUser.getEmail())
				.addDevices(devices)
				.setUsingExtension(applicationUser.isRegisteredInExtension())
				.build();
		return infoMessage;
	}


	private void assertSessionExists() {
		if (currentUser == null) {
			throw new BaseException ("Not a valid operation. User must log in with a google account");
		}
	}

	@Override
	public void registerDeviceForUser(RegisterDeviceMessage msg, String location) {
		ApplicationUser user = userManager.lookupUserByEmailAddress(msg.getEmail());
		
		if (user == null) {
			user = ApplicationUser.builder()
					.setEmail(msg.getEmail())
					.setUserId(UUID.randomUUID().toString())
					.setRegisteredInExtension(false)
					.setRegisteredLocation(location)
					.addDevice(buildDevice(msg))
					.setRegisteredDate(new Date())
					.build();
			emailService.sendNewMobileDeviceRegistrationEmail(user.getEmail());
		} else {
			MobileDevice device = null;
			if (user.getDevices() == null) {
				user.setDevices(Lists.<MobileDevice>newArrayList());
			}
			for (MobileDevice existingDevice : user.getDevices()) {
				if (existingDevice.getRegistrationId() != null
						&& existingDevice.getRegistrationId().equals(msg.getRegistrationId())) {
					device = existingDevice;
					break;
				}
			}

			if (device == null) {
				device = buildDevice(msg);
				user.getDevices().add(device);
				// Send welcome eMail
				emailService.sendNewMobileDeviceRegistrationEmail(user.getEmail());
			}

			device.setC2dmRegistrationId(msg.getC2dmHandler());
			device.setPhoneNumber(msg.getPhoneNumber());
			device.setCountry(msg.getCountry());
			device.setAuthenticated(msg.isAuthenticated());
			// The fact that we have a new request means the device is active.
			device.setLastHeardOn(new Date(System.currentTimeMillis()));
			device.setMarkedForDeletion(false);
			device.setDpVersion(msg.getDpVersion());
		}
		userManager.saveUser(user);
	}

	private MobileDevice buildDevice(RegisterDeviceMessage msg) {
		MobileDevice device = MobileDevice.builder()
				.setC2dmRegistrationId(msg.getC2dmHandler())
				.setCountry(msg.getCountry())
				.setName(msg.getName()).setPhone(true)
				.setPhoneNumber(msg.getPhoneNumber())
				.setRegistrationId(msg.getRegistrationId())
				.setAuthenticated(msg.isAuthenticated())
				.setVersion(msg.getDpVersion())
				.build();
		return device;
	}

	@Override
	public void setDeviceActivationStatus(String deviceId, boolean isActive) {
		ApplicationUser user = getApplicationUser();

		for (MobileDevice mob : user.getDevices()) {
			if (mob.getRegistrationId().equals(deviceId)) {
				mob.setAuthenticated(isActive);
				userManager.saveUser(user);
				return;
			}
		}

		throw new BaseException("Device not found for user.");
	}

	@Override
	public boolean isDeviceMarkedForDeletion(String deviceId) {
		List<MobileDevice> devices = userManager.lookupDevicebyId(deviceId);

		// If device not found, most likely its deleted.
		if (devices == null || devices.size() == 0) {
			return true;
		}

		for (MobileDevice device : devices) {
			if (device.isMarkedForDeletion()) {
				return true;
			}
		}

		return false;
	}


	@Override
	public void setDeviceMarkedForDeletion(String deviceId) {
		ApplicationUser user = getApplicationUser();

		for (MobileDevice mob : user.getDevices()) {
			if (mob.getRegistrationId().equals(deviceId)) {
				mob.setMarkedForDeletion(true);
				userManager.saveUser(user);
				return;
			}
		}

		throw new BaseException("Device not found for user.");
	}
}
