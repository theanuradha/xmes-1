package com.lahiru.chrometocall.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nullable;

import com.google.appengine.api.channel.ChannelMessage;
import com.google.appengine.api.channel.ChannelService;
import com.google.appengine.api.channel.ChannelServiceFactory;
import com.google.appengine.api.users.User;
import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.lahiru.chrometocall.common.e.BaseException;
import com.lahiru.chrometocall.common.json.IncomingSmsAlert;
import com.lahiru.chrometocall.common.json.JSONMessage;
import com.lahiru.chrometocall.common.json.MissedCallAlert;
import com.lahiru.chrometocall.entitiy.ApplicationUser;
import com.lahiru.chrometocall.entitiy.ChannelSession;
import com.lahiru.chrometocall.service.da.ApplicationUserManager;
import com.lahiru.chrometocall.service.da.MessageManager;

/**
 * Channel service implementation for the Google Channel Service.
 * @author lahiruw
 *
 */
public class GoogleChannelService implements AppChannelService {
	private static final Logger log = 
			Logger.getLogger(GoogleChannelService.class.getName());

	@Inject @Nullable User currentUser;
	@Inject ApplicationUserManager userManager;
	@Inject Gson gson;
	@Inject MessageManager messageManager;

	@Override
	public String[] createChannel(String requesterIp, String previousChannelId, String lcoation) {
		assertSessionExists();

		ApplicationUser user = userManager.lookupUserByEmailAddress(currentUser.getEmail());

		// TODO Remove previous channel

		// Channel ID is users email + appended current time ms. e.g user@gmail.com_1345666
		String channelKey = currentUser.getEmail() + Long.toString(System.currentTimeMillis());
		String token = ChannelServiceFactory.getChannelService().createChannel(channelKey, 24*60);

		ChannelSession newSession = ChannelSession.builder()
				.channelId(channelKey)
				.creationDate(new Date(System.currentTimeMillis()))
				.originatedIp(requesterIp)
				.user(currentUser.getEmail())
				.setLocation(lcoation)
				.build();

		if (user.getUserSessions() == null) {
			user.setUserSessions(new ArrayList<ChannelSession>());
		}
		user.getUserSessions().add(newSession);
		userManager.saveUser(user);
		
		return new String[] {token, channelKey};
	}

	private void assertSessionExists() {
		if (currentUser == null) {
			throw new BaseException ("Not a valid operation. User must log in with a google account");
		}
	}

	@Override
	public void alertMissedCall(MissedCallAlert alert) {
		String msg = gson.toJson(alert);
		sendMessage(msg, alert.getEmails());
	}

	@Override
	public void alertNewSms(IncomingSmsAlert alert) {
		String msg = gson.toJson(alert);
		
		try {
			messageManager.saveMessage(alert);
		} catch (Exception ex) {
			// Message not being able to save is trivial and should be ignored.
			log.warning("Could not save message: " + ex);
		}
		sendMessage(msg, alert.getEmails());
	}

	protected void sendMessage(String msg, String[] emails) {
		for (String email : emails) {
			ChannelService channelService = ChannelServiceFactory.getChannelService();
			ApplicationUser user = userManager.lookupUserByEmailAddress(email);

			if (user == null) {
				log.warning(
						String.format("User %s does not exists. Request: %s", email, msg));
			} else {
				List<ChannelSession> userSessions = user.getUserSessions();
				for (ChannelSession session: userSessions) {
					channelService.sendMessage(
							new ChannelMessage(session.getChannelId(), msg));
				}
			}
		}
	}

	@Override
	public void removeUserSession(String clientId) throws Exception {
		try {
			ChannelSession sessionToRemove = userManager.lookupChannelSession(clientId);
			ApplicationUser user = userManager.lookupUserByEmailAddress(sessionToRemove.getUser());

			Collection<ChannelSession> toRemove = Lists.newArrayList();
			Calendar cutoffTime = GregorianCalendar.getInstance();
			cutoffTime.setTimeInMillis(System.currentTimeMillis() * 1000 * 60 * 60 * 24);
			for (ChannelSession s : user.getUserSessions()) {
				if (clientId.equals(s.getChannelId()) 
						|| s.getCreationDate().before(cutoffTime.getTime())) {
					toRemove.add(s);
					break;
				}
			}
		} catch (Exception ex) {
			Logger.getLogger(GoogleChannelService.class.getName())
				.log(Level.WARNING, "Could not remove channel " + ex.getMessage());
			throw ex;
		}
	}

	@Override
	public void sendKeepAlive(String channelId) {
		ChannelService channelService = ChannelServiceFactory.getChannelService();
		
		channelService.sendMessage(
				new ChannelMessage(channelId, gson.toJson(new JSONMessage("KEEP_ALIVE", ""))));
	}
}
