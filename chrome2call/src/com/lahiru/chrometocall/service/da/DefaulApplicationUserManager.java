package com.lahiru.chrometocall.service.da;

import java.util.List;

import javax.cache.Cache;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.lahiru.chrometocall.entitiy.ApplicationUser;
import com.lahiru.chrometocall.entitiy.ChannelSession;
import com.lahiru.chrometocall.entitiy.MobileDevice;
import com.lahiru.chrometocall.guice.Cached;
import com.lahiru.chrometocall.guice.Cached.Operation;

public class DefaulApplicationUserManager implements ApplicationUserManager {

	private static final String SEARCH_BY_EMAIL 
		= "SELECT X FROM ApplicationUser X WHERE X.email=\'%s\'";
	private static final String SEARCH_SESSION_BY_CLIENTID 
		= "SELECT X FROM ChannelSession X WHERE X.channelId=\'%s\'";
	private static final String SEARCH_DEVICE_BY_ID =
			"SELECT X FROM MobileDevice X WHERE X.registrationId=\'%s\'";

	@Inject Provider<EntityManager> em;
	@Inject Cache commonCache;

	@Override
	@Cached(operationType=Operation.UPDATE, entityTpe=ApplicationUser.class)
	public ApplicationUser saveUser(ApplicationUser toUpdate) {
		ApplicationUser user = em.get().merge(toUpdate);
		return user;
	}

	@Override
	@Cached(operationType=Operation.LOAD, entityTpe=ApplicationUser.class, searchByPrimaryKey=true)
	public ApplicationUser lookupUserByEmailAddress(String email) {
		TypedQuery<ApplicationUser> emailQuery = em.get().createQuery(
				String.format(SEARCH_BY_EMAIL, email), ApplicationUser.class);
		List<ApplicationUser> list = emailQuery.getResultList();

		if (list == null || list.size() ==0) {
			return null;
		} else {
			return list.get(0);
		}
	}


	@Override
	public void removeChannelSession(ChannelSession toRemove) {
		em.get().remove(toRemove);
	}

	@Override
	public ChannelSession lookupChannelSession(String clientId) {
		TypedQuery<ChannelSession> channelQuery = em.get().createQuery(
				String.format(SEARCH_SESSION_BY_CLIENTID, clientId), ChannelSession.class);
		List<ChannelSession> list = channelQuery.getResultList();

		if (list == null || list.size() ==0) {
			return null;
		} else {
			return list.get(0);
		}
	}

	@Override
	public List<MobileDevice> lookupDevicebyId(String id) {
		// TODO - Mark for caching
		TypedQuery<MobileDevice> channelQuery = em.get().createQuery(
				String.format(SEARCH_DEVICE_BY_ID, id), MobileDevice.class);
		List<MobileDevice> list = channelQuery.getResultList();

		return list;
	}

	@Override
	public MobileDevice saveDevice(MobileDevice md) {
		em.get().merge(md);
		return md;
	}

	@Override
	/**
	 * Returns the first active device found. Please do not rely on this method to perform user specific tasks.
	 * Only use this for auditing and recording purposes.
	 */
	public MobileDevice lookupActiveDevicebyId(String id) {
		List<MobileDevice> devicebyId = lookupDevicebyId(id);

		if (devicebyId != null) {
			for (MobileDevice d : devicebyId) {
				if (!d.isMarkedForDeletion()) {
					return d;
				}
			}
		}

		return null;
	}
}
