package com.lahiru.chrometocall.service.da;

import java.util.List;

import com.google.common.collect.Lists;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.Query;
import com.lahiru.chrometocall.common.e.BaseException;
import com.lahiru.chrometocall.entitiy.Contact;
import com.lahiru.chrometocall.entitiy.ContactImage;
import com.lahiru.chrometocall.entitiy.Person;

public class ObjectifyContactManager extends AbstractObjectifyManager implements
		ContactsManager {

	@Override
	public void storeContacts(List<Contact> contacts) {
		Objectify tx = newObjTranaction();

		try {
			for (Contact c : contacts) {
				tx.put(c);
			}

			tx.getTxn().commit();
		} finally {
			if (tx.getTxn().isActive()) {
        tx.getTxn().rollback();
        throw new BaseException("Failed to store all contacts. Rolling back.");
			}
		}
	}

	@Override
	public Key<ContactImage> storeContactPicture(ContactImage image) {
		return ofy().put(image);
	}

	@Override
	public byte[] getContactPicture(String email, long contactID) {
		ContactImage contactImage = ofy().get(new Key<ContactImage>(new Key<Person>(Person.class, email), ContactImage.class,contactID));
		return contactImage.getImage();
	}

	@Override
	public List<Contact> getContactsAfter(Key<Person> person, long version) {
		Query<Contact> cursor = ofy().query(Contact.class)
				.ancestor(person)
				.filter("version > ", version);

		return Lists.newArrayList(cursor);
	}

}
