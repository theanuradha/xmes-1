package com.lahiru.chrometocall.service.da;

import com.googlecode.objectify.Key;
import com.lahiru.chrometocall.entitiy.Person;

public interface PersonManager {
	Key<Person> savePerson(Person person);

	/**
	 * Returns {@link Person} by email address.
	 */
	Person getPerson(String email);

	Key<Person> createKey(String email);
}
