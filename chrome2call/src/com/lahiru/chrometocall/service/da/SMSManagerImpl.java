package com.lahiru.chrometocall.service.da;

import com.google.common.base.Strings;
import com.google.inject.Inject;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.NotFoundException;
import com.googlecode.objectify.Objectify;
import com.lahiru.chrometocall.common.e.BaseException;
import com.lahiru.chrometocall.common.json.IncomingSmsAlert;
import com.lahiru.chrometocall.common.json.OutgoingSmsRequest;
import com.lahiru.chrometocall.entitiy.Conversation;
import com.lahiru.chrometocall.entitiy.MobileDevice;
import com.lahiru.chrometocall.entitiy.SMS;

public class SMSManagerImpl extends AbstractObjectifyManager implements
		MessageManager {

	@Inject DPConfigManager dpConfigManager;
	@Inject ApplicationUserManager userManager;

	@Override
	public SMS saveMessage(IncomingSmsAlert alert) {
		Objectify obj = super.newObjTranaction();
		try {
			if (Strings.isNullOrEmpty(alert.getPhoneId())) {
				throw new BaseException("Phone not yet registered in Deskphone.");
			}
	
			String phoneId = alert.getPhoneId();
			String otherPartyNo = alert.getNo();
			String messageContent = alert.getMessageContents();
			long time = alert.getTime();
			String contactName = alert.getContactName();
			MessageType type = MessageType.INCOMING;

			MobileDevice device = userManager.lookupActiveDevicebyId(phoneId);
	
			if (device != null) {
				return saveMessage(obj, otherPartyNo, messageContent, time,
						contactName, type, device);
			} else {
				throw new BaseException("Phone not yet registered in Deskphone.");
			}
		} finally {
			if (obj != null && obj.getTxn().isActive()) {
				obj.getTxn().rollback();
			}
		}
	}

	protected SMS saveMessage(Objectify obj, String otherPartyNo,
			String messageContent, long time, String contactName, MessageType type,
			MobileDevice device) {
		String threadId = getThreadId(device.getPhoneNumber(), otherPartyNo);
		
		Conversation thread;
		try {
			thread = obj.get(Conversation.class, threadId);
		} catch (NotFoundException e) {
			thread = new Conversation.Builder()
					.dpUserPhoneNumber(device.getPhoneNumber())
					.otherUserPhoneNumber(otherPartyNo).threadId(threadId).build();
			obj.put(thread);
		}

		Key<Conversation> convKey = new Key<Conversation>(Conversation.class, thread.getThreadId());
		SMS toSave = new SMS.Builder()
			.conversation(convKey)
			.message(messageContent)
			.id(dpConfigManager.getCurrentMessageSeqId())
			.time(time)
			.type(type.name())
			.build();

		if (contactName != null) {
			thread.setContactName(contactName);
		}

		obj.put(thread);
		obj.put(toSave);

		obj.getTxn().commit();
		
		return toSave;
	}

	/**
	 * Thread ID = User Phone number + _ + other party's phone number.
	 */
	private String getThreadId(String myNumber, String otherNumber) {
		return myNumber + "_" + otherNumber; 
	}

	@Override
	public SMS saveMessage(OutgoingSmsRequest request) {
		Objectify obj = super.newObjTranaction();
		try {
			if (Strings.isNullOrEmpty(request.getPhoneId())) {
				throw new BaseException("Phone not yet registered in Deskphone.");
			}
	
			String phoneId = request.getPhoneId();
			String otherPartyNo = request.getNo();
			String messageContent = request.getMessageContents();
			long time = request.getTime();
			MessageType type = MessageType.OUTGOING;

			MobileDevice device = userManager.lookupActiveDevicebyId(phoneId);
	
			if (device != null) {
				return saveMessage(obj, otherPartyNo, messageContent, time,
						null, type, device);
			} else {
				throw new BaseException("Phone not yet registered in Deskphone.");
			}
		} finally {
			if (obj != null && obj.getTxn().isActive()) {
				obj.getTxn().rollback();
			}
		}
	}

}
