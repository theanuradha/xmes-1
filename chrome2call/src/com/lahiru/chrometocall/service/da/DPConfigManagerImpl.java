package com.lahiru.chrometocall.service.da;

import javax.cache.Cache;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.googlecode.objectify.Objectify;
import com.lahiru.chrometocall.common.e.BaseException;
import com.lahiru.chrometocall.entitiy.DeskphoneConfigItem;

public class DPConfigManagerImpl extends AbstractObjectifyManager implements DPConfigManager {
	private static String MESSAGE_SEQUENCE_NEXT = "message_sequence_next";
	@Inject Provider<Cache> cacheProvider;

	@SuppressWarnings("unchecked")
	@Override
	public long getCurrentMessageSeqId() {
		Objectify obj =	newObjTranaction();
		long id;
		try {
			DeskphoneConfigItem nextSequence = 
					(DeskphoneConfigItem) cacheProvider.get().get(MESSAGE_SEQUENCE_NEXT);

			if (nextSequence == null) {
				// Check the database
				try {
					nextSequence = obj.get(DeskphoneConfigItem.class, MESSAGE_SEQUENCE_NEXT);
				} catch (Exception e) {
					nextSequence = new DeskphoneConfigItem(MESSAGE_SEQUENCE_NEXT, "0");
				}
			}

			id = Long.parseLong(nextSequence.getValue()) + 1;
			nextSequence.setValue(Long.toString(id));
			obj.put(nextSequence);

			obj.getTxn().commit();
			cacheProvider.get().put(MESSAGE_SEQUENCE_NEXT, nextSequence);
		} catch (Exception ex) {
			throw new BaseException("Objectify: Could not load next sequence id");
		} finally {
			if (obj != null && obj.getTxn().isActive()) {
				obj.getTxn().rollback();
			}
		}

		return id;
	}
}
