package com.lahiru.chrometocall.service.da;

import java.util.List;

import com.googlecode.objectify.Key;
import com.lahiru.chrometocall.entitiy.Contact;
import com.lahiru.chrometocall.entitiy.ContactImage;
import com.lahiru.chrometocall.entitiy.Person;

public interface ContactsManager {
	/**
	 * Stores a list of contacts
	 * @param person
	 * @param contacts
	 */
	void storeContacts(List<Contact> contacts);

	Key<ContactImage> storeContactPicture(ContactImage image);

	byte[] getContactPicture(String email, long contactID);

	List<Contact> getContactsAfter(Key<Person> person, long version);
}
