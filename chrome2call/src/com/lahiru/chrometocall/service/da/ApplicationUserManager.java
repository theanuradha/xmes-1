package com.lahiru.chrometocall.service.da;

import java.util.List;

import com.lahiru.chrometocall.entitiy.ApplicationUser;
import com.lahiru.chrometocall.entitiy.ChannelSession;
import com.lahiru.chrometocall.entitiy.MobileDevice;

/**
 * Manage {@link ApplicationUser} accounts.
 * @author lahiruw
 *
 */
public interface ApplicationUserManager {
	/**
	 * Retrieve {@link ApplicationUser} by the email address.
	 */
	ApplicationUser lookupUserByEmailAddress(String email);

	/**
	 * Saves (update if exists) the {@link ApplicationUser} object.
	 * @return reference to the created object.
	 */
	ApplicationUser saveUser(ApplicationUser toUpdate);

	ChannelSession lookupChannelSession(String clientId);

	List<MobileDevice> lookupDevicebyId(String id);

	MobileDevice lookupActiveDevicebyId(String id);

	MobileDevice saveDevice(MobileDevice md);

	void removeChannelSession(ChannelSession toRemove);
}
