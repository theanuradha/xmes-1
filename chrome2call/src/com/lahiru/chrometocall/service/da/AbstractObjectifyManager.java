package com.lahiru.chrometocall.service.da;

import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import com.lahiru.chrometocall.entitiy.Contact;
import com.lahiru.chrometocall.entitiy.ContactImage;
import com.lahiru.chrometocall.entitiy.Conversation;
import com.lahiru.chrometocall.entitiy.DeskphoneConfigItem;
import com.lahiru.chrometocall.entitiy.Person;
import com.lahiru.chrometocall.entitiy.SMS;

public abstract class AbstractObjectifyManager {
	static {
		ObjectifyService.register(DeskphoneConfigItem.class);
		ObjectifyService.register(SMS.class);
		ObjectifyService.register(Conversation.class);
		ObjectifyService.register(Person.class);
		ObjectifyService.register(Contact.class);
		ObjectifyService.register(ContactImage.class);
	}

	protected final Objectify newObjTranaction() {
		return ObjectifyService.beginTransaction();
	}

	protected final Objectify ofy() {
		return ObjectifyService.begin();
	}
}
