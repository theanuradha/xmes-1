package com.lahiru.chrometocall.service.da;

/**
 * CRUD operations for Deskphone config
 * @author lahiruw
 *
 */
public interface DPConfigManager {
	long getCurrentMessageSeqId();
}
