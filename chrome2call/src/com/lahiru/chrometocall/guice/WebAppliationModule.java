package com.lahiru.chrometocall.guice;

import com.google.inject.Singleton;
import com.google.inject.persist.PersistFilter;
import com.google.inject.persist.jpa.JpaPersistModule;
import com.google.inject.servlet.ServletModule;
import com.lahiru.chrometocall.extension.app.AppIndexServlet;
import com.lahiru.chrometocall.extension.app.AppLoginServlet;
import com.lahiru.chrometocall.extension.CallRequestServlet;
import com.lahiru.chrometocall.extension.ContactPictureServlet;
import com.lahiru.chrometocall.extension.EstablishChannelServlet;
import com.lahiru.chrometocall.extension.ExtensionAuthenticationFilter;
import com.lahiru.chrometocall.extension.GetContactsServlet;
import com.lahiru.chrometocall.extension.GetUserConfigServlet;
import com.lahiru.chrometocall.extension.SMSRequestServlet;
import com.lahiru.chrometocall.extension.SendHeartBeatServlet;
import com.lahiru.chrometocall.extension.UpdateDeviceServlet;
import com.lahiru.chrometocall.extension.UserRegistrationServlet;
import com.lahiru.chrometocall.extension.app.AppAuthenticationFilter;
import com.lahiru.chrometocall.mobile.CheckRegisteredAccountsServlet;
import com.lahiru.chrometocall.mobile.ContactSyncServlet;
import com.lahiru.chrometocall.mobile.IncomingSmsAlertServlet;
import com.lahiru.chrometocall.mobile.MissedCallAlertServlet;
import com.lahiru.chrometocall.mobile.PingAcknowledgeServlet;
import com.lahiru.chrometocall.mobile.RegisterDeviceServlet;
import com.lahiru.chrometocall.servlet.StaticContentHostServlet;
import com.lahiru.chrometocall.servlet.util.ChannelPresenceServlet;
import com.lahiru.chrometocall.web.DeviceServlet;
import com.lahiru.chrometocall.web.UserHomeServlet;
import com.lahiru.chrometocall.web.WebAuthenticationFilter;
import com.lahiru.chrometocall.web.admin.AdminServlet;
import com.lahiru.chrometocall.web.admin.CleanupOldSessions;
import com.lahiru.chrometocall.web.admin.FixEmails;
import com.lahiru.chrometocall.web.admin.PingAllPhones;

/**
 * Configure Servlets and filters used by the chrome extension.
 * @author lahiru
 */
public class WebAppliationModule extends ServletModule{
	public static final String EXTENSION_ROOT = "/extension/";
	public static final String HANDLE_NEW_USER = EXTENSION_ROOT + "handlenewuser";
	public static final String GET_USER_CONFIG = EXTENSION_ROOT + "getuserconfig";
	public static final String REQUEST_CALL = EXTENSION_ROOT + "requestCall";
	public static final String ESTABLISH_CHANNEL = EXTENSION_ROOT + "establishchannel";
	public static final String SEND_SMS = EXTENSION_ROOT + "sendSms";
	public static final String SEND_KEEPALIVE = EXTENSION_ROOT + "heartBeat";
	public static final String CONTACT_PICTURE = EXTENSION_ROOT + "contact";
	public static final String GET_NEW_CONTACTS = EXTENSION_ROOT + "newcontacts";
	public static final String UPDATE_DEVICE = EXTENSION_ROOT + "updateDevice";

	public static final String MOBILE_ROOT = "/mobile/";
	public static final String CHECK_ACCOUNT_REGISTERED = MOBILE_ROOT + "isregistered";
	public static final String REGISTER_DEVICE = MOBILE_ROOT + "registerPhone";
	public static final String NEW_MISSED_CALL = MOBILE_ROOT + "missedcall";
	public static final String NEW_INCOMING_SMS = MOBILE_ROOT + "onsms";
	public static final String PING_ACKNOWLEDGE = MOBILE_ROOT + "pingAcknowledge";
	public static final String CONTACT_SYNC = MOBILE_ROOT + "syncContacts";

	public static final String WEB_DEVICE_LIST = "/d";
	public static final String WEB_INDEX = "/";

	public static final String WEB_ADMIN_INDEX = "/admin";
	public static final String TASK_ROOT = "/tasks/";
	public static final String TASK_DELETE_OLD_SESSIONS = TASK_ROOT + "delete_session";
	public static final String TASK_PING_ALL_PHONES = TASK_ROOT + "ping_phones";
	public static final String TASK_FIX_EMAIL_ADDRESSES = TASK_ROOT + "fix_emails";

	public static final String APP_ROOT = "/app/";
	public static final String APP_LOGIN_PAGE = APP_ROOT + "login";

	@Override
	protected void configureServlets() {
		install(new JpaPersistModule("transactions-optional"));

		bind(StaticContentHostServlet.class).in(Singleton.class);
		bind(GetUserConfigServlet.class).in(Singleton.class);
		bind(UserRegistrationServlet.class).in(Singleton.class);
		bind(ExtensionAuthenticationFilter.class).in(Singleton.class);
		bind(AppAuthenticationFilter.class).in(Singleton.class);
		bind(CheckRegisteredAccountsServlet.class).in(Singleton.class);
		bind(RegisterDeviceServlet.class).in(Singleton.class);
		bind(CallRequestServlet.class).in(Singleton.class);
		bind(EstablishChannelServlet.class).in(Singleton.class);
		bind(MissedCallAlertServlet.class).in(Singleton.class);
		bind(ChannelPresenceServlet.class).in(Singleton.class);
		bind(DeviceServlet.class).in(Singleton.class);
		bind(WebAuthenticationFilter.class).in(Singleton.class);
		bind(UserHomeServlet.class).in(Singleton.class);
		bind(IncomingSmsAlertServlet.class).in(Singleton.class);
		bind(ContactSyncServlet.class).in(Singleton.class);
		bind(SMSRequestServlet.class).in(Singleton.class);
		bind(AdminServlet.class).in(Singleton.class);
		bind(PingAcknowledgeServlet.class).in(Singleton.class);
		bind(SendHeartBeatServlet.class).in(Singleton.class);
		bind(ContactPictureServlet.class).in(Singleton.class);
		bind(GetContactsServlet.class).in(Singleton.class);
		bind(AppLoginServlet.class).in(Singleton.class);
		bind(UpdateDeviceServlet.class).in(Singleton.class);

		bind(AppIndexServlet.class).in(Singleton.class);

		bind(CleanupOldSessions.class).in(Singleton.class);
		bind(PingAllPhones.class).in(Singleton.class);
		bind(FixEmails.class).in(Singleton.class);

		// Intercept requests from extension and verify the user is logged in.
		filter(EXTENSION_ROOT + "*").through(ExtensionAuthenticationFilter.class);
		filterWebRequests();
		// Guice JPA filter.
		filter(APP_ROOT + "*").through(AppAuthenticationFilter.class);
		filter("/*").through(PersistFilter.class);

		//Post log-in page.
		serve(StaticContentHostServlet.SUCCESSFUL_LOGGEDIN_PAGE).with(StaticContentHostServlet.class);

		//Get user configuration.
		serve(GET_USER_CONFIG).with(GetUserConfigServlet.class);
		// Handle user registration.
		serve(HANDLE_NEW_USER).with(UserRegistrationServlet.class);
		// Request a phone call coming from extension
		serve(REQUEST_CALL).with(CallRequestServlet.class);
		//Check google account registration?
		serve(CHECK_ACCOUNT_REGISTERED).with(CheckRegisteredAccountsServlet.class);
		// Register a device on the system.
		serve(REGISTER_DEVICE).with(RegisterDeviceServlet.class);
		// Establish a channel connection.
		serve(ESTABLISH_CHANNEL).with(EstablishChannelServlet.class);
		// Missed call alert from phone.
		serve(NEW_MISSED_CALL).with(MissedCallAlertServlet.class);
		// Incoming SMS on the phone
		serve(NEW_INCOMING_SMS).with(IncomingSmsAlertServlet.class);
		// Contact sync
		serve(CONTACT_SYNC).with(ContactSyncServlet.class);
		// Outgoing SMS to the phone
		serve(SEND_SMS).with(SMSRequestServlet.class);
		/* Device list page. */
		serve(WEB_DEVICE_LIST).with(DeviceServlet.class);
		/* User Home */
		serve(WEB_INDEX).with(UserHomeServlet.class);
		/* Admin Page */
		serve(WEB_ADMIN_INDEX).with(AdminServlet.class);
		/* Acknowledge Ping */
		serve(PING_ACKNOWLEDGE).with(PingAcknowledgeServlet.class);
		/* Heartbeat / Keepalive */
		serve(SEND_KEEPALIVE).with(SendHeartBeatServlet.class);
		/* Contact picture host */
		serve(CONTACT_PICTURE).with(ContactPictureServlet.class);
		/* Get new contacts */
		serve(GET_NEW_CONTACTS).with(GetContactsServlet.class);
		/* Update device status */
		serve(UPDATE_DEVICE).with(UpdateDeviceServlet.class);

		/* APP */
		serve(APP_ROOT).with(AppIndexServlet.class);
		serve(APP_LOGIN_PAGE).with(AppLoginServlet.class);

		/*tasks*/
		serve(TASK_DELETE_OLD_SESSIONS).with(CleanupOldSessions.class);
		serve(TASK_PING_ALL_PHONES).with(PingAllPhones.class);
		serve(TASK_FIX_EMAIL_ADDRESSES).with(FixEmails.class);
		
		serve("/_ah/channel/disconnected/").with(ChannelPresenceServlet.class);
	}

	private void filterWebRequests() {
		filter(WEB_DEVICE_LIST).through(WebAuthenticationFilter.class);
	}
}
