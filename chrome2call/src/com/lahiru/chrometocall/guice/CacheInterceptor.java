package com.lahiru.chrometocall.guice;

import java.util.Collection;

import javax.cache.Cache;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

import com.google.inject.Provider;
import com.lahiru.chrometocall.entitiy.ApplicationUser;
import com.lahiru.chrometocall.guice.Cached.Operation;

public class CacheInterceptor implements MethodInterceptor {
	private final String USER_PREFIX = "user_";

	Provider<Cache> cacheProvider;

	public CacheInterceptor(Provider<Cache> cacheProvider) {
		this.cacheProvider = cacheProvider;
	}

	@Override
	public Object invoke(MethodInvocation method) throws Throwable {
		Cached cached = method.getMethod().getAnnotation(Cached.class);

		if (cached.entityTpe() == ApplicationUser.class) {
			return handleApplicationUserOperation(cached, method);
		} else {
			return method.proceed();
		}
	}

	@SuppressWarnings("unchecked")
	private Object handleApplicationUserOperation(Cached cached,
			MethodInvocation method) throws Throwable {
		if (cached.operationType() == Operation.LOAD && cached.searchByPrimaryKey()) {
			String email = (String) method.getArguments()[0];

			// All email addresses should be stored as lower case.
			if (email != null) {
				email = email.toLowerCase();
			}

			ApplicationUser user = (ApplicationUser) cacheProvider.get().get(USER_PREFIX + email);
			if (user != null) {
				return user;
			} else {
				method.getArguments()[0] = email;
				user = (ApplicationUser) method.proceed();

				if (user != null) {
					cacheProvider.get().put(USER_PREFIX+email, user);
				}
				return user;
			}
		} else if (cached.operationType() == Operation.UPDATE) {
			ApplicationUser saving = (ApplicationUser) method.proceed();

			if (saving.getEmail() != null) {
				saving.setEmail(saving.getEmail().toLowerCase());
			}

			cacheProvider.get().put(USER_PREFIX+saving.getEmail(), saving);
			return saving;
		} else {
			Object object = method.proceed();
			if (object instanceof Collection<?>) {
				Collection<ApplicationUser> users = (Collection<ApplicationUser>) object;
				for (ApplicationUser u : users) {
					cacheProvider.get().put(USER_PREFIX+u.getEmail(), u);
				}
			}
			return object;
		}
	}
}
