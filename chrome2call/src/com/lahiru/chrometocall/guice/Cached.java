package com.lahiru.chrometocall.guice;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * DAO methods, where saved/retrieved data are cached.
 *
 * @author lahiruw
 *
 */
@Retention(RetentionPolicy.RUNTIME) 
@Target(ElementType.METHOD)
public @interface Cached {
	public enum Operation {LOAD, UPDATE}
	Operation operationType() default Operation.LOAD;
	@SuppressWarnings("rawtypes")
	Class entityTpe();
	boolean searchByPrimaryKey() default false;
}
