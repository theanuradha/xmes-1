package com.lahiru.chrometocall.guice;

import javax.cache.Cache;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.google.inject.matcher.Matchers;
import com.google.inject.servlet.RequestScoped;
import com.lahiru.chrometocall.service.AppChannelService;
import com.lahiru.chrometocall.service.ContactsService;
import com.lahiru.chrometocall.service.EmailService;
import com.lahiru.chrometocall.service.EmailServiceImpl;
import com.lahiru.chrometocall.service.GoogleChannelService;
import com.lahiru.chrometocall.service.ContactServiceImpl;
import com.lahiru.chrometocall.service.TalkToPhoneService;
import com.lahiru.chrometocall.service.TalkToPhoneServiceImpl;
import com.lahiru.chrometocall.service.UserInfoService;
import com.lahiru.chrometocall.service.UserInfoServiceImpl;
import com.lahiru.chrometocall.service.da.ApplicationUserManager;
import com.lahiru.chrometocall.service.da.ContactsManager;
import com.lahiru.chrometocall.service.da.DPConfigManager;
import com.lahiru.chrometocall.service.da.DPConfigManagerImpl;
import com.lahiru.chrometocall.service.da.DefaulApplicationUserManager;
import com.lahiru.chrometocall.service.da.MessageManager;
import com.lahiru.chrometocall.service.da.ObjectifyContactManager;
import com.lahiru.chrometocall.service.da.ObjectifyPersonManager;
import com.lahiru.chrometocall.service.da.PersonManager;
import com.lahiru.chrometocall.service.da.SMSManagerImpl;

public class ServiceModule extends AbstractModule {

	@Override
	protected void configure() {
		CacheInterceptor cacheInterceptor = new CacheInterceptor(
				getProvider(Cache.class));
		bindInterceptor(
				Matchers.any(), Matchers.annotatedWith(Cached.class), cacheInterceptor);

		//Services
		bind(UserInfoService.class).to(UserInfoServiceImpl.class).in(RequestScoped.class);
		bind(TalkToPhoneService.class).to(TalkToPhoneServiceImpl.class).in(RequestScoped.class);
		bind(AppChannelService.class).to(GoogleChannelService.class).in(RequestScoped.class);
		bind(EmailService.class).to(EmailServiceImpl.class).in(Singleton.class);
		bind(ContactsService.class).to(ContactServiceImpl.class).in(RequestScoped.class);

		//Managers
		bind(ApplicationUserManager.class).to(DefaulApplicationUserManager.class).in(Singleton.class);
		bind(DPConfigManager.class).to(DPConfigManagerImpl.class).in(Singleton.class);
		bind(MessageManager.class).to(SMSManagerImpl.class).in(Singleton.class);
		bind(PersonManager.class).to(ObjectifyPersonManager.class).in(Singleton.class);
		bind(ContactsManager.class).to(ObjectifyContactManager.class).in(Singleton.class);
	}
}
