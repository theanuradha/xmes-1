package com.lahiru.xmes;

import java.util.ArrayList;
import java.util.Map;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.lahiru.xmes.data.CallRequest;
import com.lahiru.xmes.data.CallRequestDataHandler;
import com.lahiru.xmes.service.ApplicationPreferences;
import com.lahiru.xmes.service.CallQueue;
import com.lahiru.xmes.service.TelephonyService;

public class CallRequestNotifyActivity extends SchedulableCallActivity implements
		OnClickListener, OnCheckedChangeListener {

	public static final String SCHEDULED_CALL_REQUEST_ID = "call_request_id";
	public static boolean IS_DIALOG_DISPLAYED = false;
	private CallRequest s;
	private Map<String, Long> remindOptions;

	private TextView numberToCall;
	private TextView txtCallNotes;
	private CheckBox isSnooze;
	private Button okButton;
	private Button discardButton;
	private ToggleButton tbRemindIn;
	private ToggleButton tbRemindAt;
	private RelativeLayout notificationTimeSetPanel;
	private Spinner spHomeRemindTimes;
	private Button btnSetTime;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			// Check if this is a scheduled one.
			String crId = getIntent().getStringExtra(SCHEDULED_CALL_REQUEST_ID);
			if (crId != null) {
				s = CallRequestDataHandler.INSTANCE(this).findCallRequest(crId);
			}

			if (s == null) {
				s = CallQueue.peek(this);
			}

			IS_DIALOG_DISPLAYED = true;
			showUI(s, !CallQueue.isQueueEmpty(this));
			
		} catch (Exception ex) {
			ErrorHandler.handleUiError(this, ex);
		}
	}

	private void showUI(CallRequest req, boolean moreRequestsInQueue) {
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.newcallalert);

		numberToCall = (TextView) findViewById(R.id.numberToCall);
		isSnooze = (CheckBox) findViewById(R.id.remind_later_checkbox);
		okButton = (Button) findViewById(R.id.ok_button);
		discardButton = (Button) findViewById(R.id.ignore_button);
		txtCallNotes = (TextView) findViewById(R.id.txtCallNotes);
		spHomeRemindTimes = (Spinner) findViewById(R.id.spAlertRemindTimes);
		tbRemindIn = (ToggleButton) findViewById(R.id.tbAlertRemindIn);
		tbRemindAt = (ToggleButton) findViewById(R.id.tbAlertRemindAt);
		btnSetTime = (Button) findViewById(R.id.btnAlertSetTime);
		notificationTimeSetPanel = (RelativeLayout) findViewById(R.id.notificationTimeSetPanel);

		showHideRemindTimePanel(View.GONE);
		selectRemindIn();

		remindOptions = ApplicationPreferences.getRemindOptions();
		ArrayAdapter<String> remindAdapter = new ArrayAdapter<String>(
				this, android.R.layout.simple_spinner_item, new ArrayList<String>(remindOptions.keySet()));
		remindAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spHomeRemindTimes.setAdapter(remindAdapter);

		numberToCall.setText(String.format("Call %s ?", req.getNumber()));
		txtCallNotes.setText(String.format("\"%s\"", s.getNote()));
		isSnooze.setOnCheckedChangeListener(this);
		okButton.setOnClickListener(this);
		discardButton.setOnClickListener(this);

		btnSetTime.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showDialog(TIME_DIALOG_ID);
			}
		});

		tbRemindAt.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				selectRemindAt();
			}
		});
		tbRemindIn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				selectRemindIn();
			}
		});
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if (buttonView == isSnooze) {
			okButtonText(isChecked);
			int showHide = (isChecked)? View.VISIBLE : View.GONE;
			showHideRemindTimePanel(showHide);

			if (!isChecked) {
				okButton.setEnabled(true);
			} else {
				selectRemindIn();
			}
		}
	}

	private void showHideRemindTimePanel(int showHide) {
		tbRemindAt.setVisibility(showHide);
		tbRemindIn.setVisibility(showHide);
		notificationTimeSetPanel.setVisibility(showHide);
		
	}

	private void okButtonText(boolean isChecked) {
		if (isChecked) {
			okButton.setText(R.string.alert_confirm_snooze);
		} else {
			okButton.setText(R.string.alert_confirm_call);
		}
	}

	@Override
	public void onClick(View v) {
		try {
			if (v == okButton) {
				if (isSnooze.isChecked()) { // Re-schedule
					long nextReminder;
					if (tbRemindIn.isChecked()) {
						nextReminder = remindOptions.get(
								spHomeRemindTimes.getSelectedItem()) + System.currentTimeMillis();
					} else {
						nextReminder = lastSetTime.getTimeInMillis();
					}
					scheduleCall(nextReminder, s);
				} else {
					TelephonyService.makePhoneCall(this, s);
					s.setType(CallRequest.TYPE_FINISHED);
					CallRequestDataHandler.INSTANCE(this).update(s);
				}
			} else if (v == discardButton) {
				s.setType(CallRequest.TYPE_DELETED);
				CallRequestDataHandler.INSTANCE(this).update(s);
			}
		} catch (Exception e) {
			ErrorHandler.handleUiError(this, e);
		} finally {
			IS_DIALOG_DISPLAYED = false;
			TelephonyService.processNextCall(CallRequestNotifyActivity.this);
			TelephonyService.updateScheduledCallNotification(this);
			finish();
		}
	}

	protected void selectRemindIn() {
		tbRemindIn.setChecked(true);
		tbRemindAt.setChecked(false);
		spHomeRemindTimes.setVisibility(View.VISIBLE);
		btnSetTime.setVisibility(View.GONE);
		okButton.setEnabled(true);
	}
	protected void selectRemindAt() {
		tbRemindIn.setChecked(false);
		tbRemindAt.setChecked(true);
		spHomeRemindTimes.setVisibility(View.GONE);
		btnSetTime.setVisibility(View.VISIBLE);
		okButton.setEnabled((lastSetTime != null));
	}

	@Override
	protected Button getSetTimeButton() {
		return btnSetTime;
	}

	@Override
	protected Button getConfirmButton() {
		return okButton;
	}
}
