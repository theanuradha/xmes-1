package com.lahiru.xmes;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.view.View;

import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.lahiru.xmes.GCMIntentService.AccountStatusChangeListener;
import com.lahiru.xmes.service.DeskPhoneAccountService;

public abstract class AbstractDeskphoneActivity extends AbstractActivity implements
		AccountStatusChangeListener {

	@Override
	protected void onPause() {
		super.onPause();
		GCMIntentService.removeAccountStatusChangeListener(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		GCMIntentService.addAccountStatusChangeListener(this);

		// Load ad
		AdRequest adRequest = new AdRequest();
		LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		Location location = locationManager
				.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		adRequest.setLocation(location);
		getAdView().loadAd(adRequest);
	}

	protected abstract AdView getAdView();

	@Override
	public void onAccountStatusChanged(String email, boolean isActive) {
		Runnable activationStatusChecker = new Runnable() {
			@Override
			public void run() {
				try {
					boolean activated = DeskPhoneAccountService.
							isActivatedLocally(AbstractDeskphoneActivity.this);
					final int visibilityStatus = activated? View.INVISIBLE : View.VISIBLE;
					getUiHandler().post(new Runnable() {
						@Override
						public void run() {
							getDectivatedPopup().setVisibility(visibilityStatus);
						}
					});
				} catch (Exception e) {
					ErrorHandler.handleUiError(AbstractDeskphoneActivity.this, e);
				}
			}
		};
		new Thread(activationStatusChecker).start();
	}

	protected abstract View getDectivatedPopup();
}
