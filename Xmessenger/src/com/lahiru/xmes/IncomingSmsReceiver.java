package com.lahiru.xmes;

import java.util.HashMap;
import java.util.Map;

import com.lahiru.xmes.service.ContactsHelper;
import com.lahiru.xmes.service.DeviceManager;
import com.lahiru.xmes.service.TelephonyService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

public class IncomingSmsReceiver extends BroadcastReceiver {
	private static String previousMessage = null;
	@Override
	public void onReceive(Context ctx, Intent intent) {
		if(intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")){
			Bundle bundle = intent.getExtras();
			Map<String, String> messages = new HashMap<String, String>();

			DeviceManager deviceManager = DeviceManager.forContext(ctx);
			deviceManager.checkUpdateStatus();
			ContactsHelper.startSync(ctx);

			if (!deviceManager.isSmsNotificationEnabled()) {
				return;
			}

			if (bundle != null) {
				Object[] pdus = (Object[]) bundle.get("pdus");
        for(int i=0; i<pdus.length; i++){
        	SmsMessage msg = SmsMessage.createFromPdu((byte[])pdus[i]);
          String msg_from = msg.getOriginatingAddress();
          String msgBody = msg.getMessageBody();

          if ((msg_from + msgBody).equals(previousMessage)) {
          	continue;
          }

          previousMessage = msg_from + msgBody;
          String currentMessage = messages.get(msg_from);
          if (currentMessage == null) {
          	messages.put(msg_from, msgBody);
          } else {
          	messages.put(msg_from, currentMessage + msgBody);
          }
        }
			}

			for (String from: messages.keySet()) {
				TelephonyService.onMessageReceived(ctx, from, messages.get(from));
			}
		}
	}

}
