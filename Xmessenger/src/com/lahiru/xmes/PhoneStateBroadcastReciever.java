package com.lahiru.xmes;

import org.apache.http.NameValuePair;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

import com.lahiru.xmes.service.ContentHelper;
import com.lahiru.xmes.service.DeskPhoneAccountService;
import com.lahiru.xmes.service.DeviceManager;
import com.lahiru.xmes.service.TelephonyService;

public class PhoneStateBroadcastReciever extends BroadcastReceiver {
	private static boolean LAST_INCOMING_CALL_ACK = false;
	private static String LAST_INCOMING_NUMBER = null;

	@Override
	public void onReceive(final Context context, Intent intent) {
		try {
			if (!DeskPhoneAccountService.isActivatedLocally(context)) {
				return;
			}
			DeviceManager.forContext(context).checkUpdateStatus();
		} catch (Exception e) {
			ErrorHandler.handleNonUiError(e);
		}
		onPhoneIdle(context);
	}

	private void onPhoneIdle(final Context context) {
		TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
    telephonyManager.listen(new PhoneStateListener() {
			@Override
			public void onCallStateChanged(int state, String incomingNumber) {
				super.onCallStateChanged(state, incomingNumber);

				if (state == TelephonyManager.CALL_STATE_IDLE) {
					if (LAST_INCOMING_CALL_ACK) {
						NameValuePair byNumber = ContentHelper
						.getContactDisplayNameByNumber(context, incomingNumber);
						String callerName = byNumber.getName();
						String callerContactId = byNumber.getValue();
						// Tell server about the new missed call.

						if (DeviceManager.forContext(context).isCallNotificationEnabled()) {
							TelephonyService.onMissedCall(context, LAST_INCOMING_NUMBER, callerName, callerContactId);
						}
						
					} 
					LAST_INCOMING_CALL_ACK = false;
				} else if (state == TelephonyManager.CALL_STATE_RINGING) {
					if (!LAST_INCOMING_CALL_ACK) {
						LAST_INCOMING_CALL_ACK = true;
						LAST_INCOMING_NUMBER = incomingNumber;
					}
				} else {
					LAST_INCOMING_CALL_ACK = false;
				}
			}
    }, PhoneStateListener.LISTEN_CALL_STATE);
	}

}
