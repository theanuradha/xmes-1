package com.lahiru.xmes.util;

public class Utilities {
	public static String santizePhoneNumber(String number) {
		String sanitized = number;
		if (number == null || number.length() < 1) {
			return number; // Leave it for now Will get into trouble later.
		}

		// + in the front tends to get dropped during the transport.
		if (number.charAt(0) == ' ') {
			sanitized = number.replaceFirst(" ", "+");
		}

		return sanitized;
	}
}
