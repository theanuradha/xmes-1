package com.lahiru.xmes;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import android.accounts.Account;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.lahiru.xmes.GCMIntentService.AccountStatusChangeListener;
import com.lahiru.xmes.exception.InvalidCDMCodeException;
import com.lahiru.xmes.service.ContactsHelper;
import com.lahiru.xmes.service.DeskPhoneAccountService;
import com.lahiru.xmes.service.DeviceManager;
import com.lahiru.xmes.service.TelephonyService;

public class Home extends AbstractActivity implements OnClickListener,
		AccountStatusChangeListener {
	ImageView ivLogo;
	RelativeLayout main;
	private LinearLayout lLoginLayout;
	private LinearLayout buttonPanel;
	private LinearLayout phoneNoView;
	private TextView statusMessage;
	private EditText txtPhoneNumber;
	private Button btnPhoneVerify;
	private TextView tvActivationLinkl;
	private LinearLayout activationRequiredView;
	private LinearLayout errorMessageWindow;
	private Button errorButton;
	private Stage failBackStage = Stage.START;

	DeviceManager dm;
	boolean authenticated = false;
	private String country;
	String numberBeforeEdit = "";
	private PhoneNumberUtil phoneNumberUtil;
	private String verifiedPhoneNumber;
	private String email;

	Map<Button, Account> accountButtons;

	private int height;
	@SuppressWarnings("unused")
	private int width;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		init();

		performDeskPhoneInit();
		
	}

	@SuppressWarnings("deprecation")
	@SuppressLint("DefaultLocale")
	private void init() {
		setContentView(R.layout.home_layout);
		accountButtons = new HashMap<Button, Account>();
		ivLogo = (ImageView) findViewById(R.id.logo);
		main = (RelativeLayout) findViewById(R.id.home_container);
		lLoginLayout = (LinearLayout) findViewById(R.id.loginview);
		buttonPanel = (LinearLayout) findViewById(R.id.buttonPanel);
		statusMessage = (TextView) findViewById(R.id.statusMessage);
		phoneNoView = (LinearLayout) findViewById(R.id.phoneNoView);
		txtPhoneNumber = (EditText) findViewById(R.id.txtPhoneNumber);
		btnPhoneVerify = (Button) findViewById(R.id.btnVerifyNumber);
		tvActivationLinkl = (TextView) findViewById(R.id.activationLink);
		activationRequiredView = (LinearLayout) findViewById(R.id.activationRequiredView);
		errorMessageWindow = (LinearLayout) findViewById(R.id.errormessageWindow);
		errorButton = (Button) findViewById(R.id.btnErorOk);

		dm = DeviceManager.forContext(this);
		country = dm.getCountry();
		country = (country != null) ? country.toUpperCase() : "US";
		phoneNumberUtil = PhoneNumberUtil.getInstance();
		height = getWindowManager().getDefaultDisplay().getHeight();
		width = getWindowManager().getDefaultDisplay().getWidth();
		btnPhoneVerify.setOnClickListener(this);
		tvActivationLinkl.setMovementMethod(LinkMovementMethod.getInstance());
		errorButton.setOnClickListener(this);

		GCMIntentService.addAccountStatusChangeListener(this);

		RelativeLayout.LayoutParams layoutParams =
				(android.widget.RelativeLayout.LayoutParams) ivLogo.getLayoutParams();
		int topMargin = height * 1/6;
		layoutParams.setMargins(0, topMargin, 0, 0);
		ivLogo.setLayoutParams(layoutParams);
	}


	private void performDeskPhoneInit() {
		Runnable initializer = new Runnable() {
			@Override
			public void run() {
				try {
					dm.registerIfNew();
					if (!DeskPhoneAccountService.syncAccount(Home.this)) {
						failBackStage = Stage.START;
						showErrorDialog();
						return;
					}

					Map<String, Boolean> registeredAccounts = DeskPhoneAccountService.loadAccounts(Home.this);

					// Scenario 01 - No accounts
					if (registeredAccounts == null || registeredAccounts.keySet().size() == 0) {
						Account[] accounts = DeskPhoneAccountService.getAccounts(Home.this);
						renderAccounts(accounts);
						
					// Scenario 02 - Account selected but the phone number hasnt been verified.
					} else if (dm.getRegisteredPhoneNumber() == null ||
							dm.getRegisteredPhoneNumber().trim().length() == 0) {
						handlePhoneNumberValidation();
						
					// Scenario 3 - Check for the activation and if all good take to the main app.
					} else {
						checkDeviceActivationStatus();
					}

				} catch (Exception e) {
					ErrorHandler.handleUiError(Home.this, e);
				}
			}
		};
		Thread initThread = new Thread(initializer);
		initThread.start();
	}

	protected void renderAccounts(final Account[] accounts) {
		getUiHandler().post(new Runnable() {
			@Override
			public void run() {
				lLoginLayout.setVisibility(View.VISIBLE);
				for (Account account : accounts) {
					Button forAccount = new Button(Home.this);
					forAccount.setTag("GACCOUNT_" + account.name);
					forAccount.setVisibility(View.VISIBLE);
					forAccount.setText(account.name);
					LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
							(int) convertDpToPixel(220, Home.this),
							(int) convertDpToPixel(50, Home.this));
					forAccount.setBackgroundResource(R.drawable.login_user_account_button);
					forAccount.setTextColor(Color.rgb(17, 84, 202));
					forAccount.setLayoutParams(params);
					forAccount.setOnClickListener(Home.this);
					buttonPanel.addView(forAccount);
					accountButtons.put(forAccount, account);
				}
			}
		});
	}

	@Override
	public void onClick(View v) {
		final Account selected = accountButtons.get(v);
		if (v instanceof Button && selected != null) {
			Home.this.email = selected.name;
			// Register the account.
			lLoginLayout.setVisibility(View.INVISIBLE);
			statusMessage.setVisibility(View.VISIBLE);
			statusMessage.setText(R.string.home_registeringaccount);
			Runnable accountRegistrar = new Runnable() {
				@Override
				public void run() {
					authenticated = DeskPhoneAccountService.authenticate(selected, Home.this);
					if (!authenticated) {
						Home.this.getUiHandler().post(new Runnable() {
							@Override
							public void run() {
								Toast toast = Toast.makeText(
										Home.this,
										"Failed to authenticate " + selected.name + " manual activation required.",
										Toast.LENGTH_LONG);
								toast.show();
							}
						});
					}
					// Next step: phone number.
					handlePhoneNumberValidation();
				}
			};
			Thread t = new Thread(accountRegistrar);
			t.start();
		} else if (v == btnPhoneVerify) {
			PhoneNumber phoneNumber;
			try {
				phoneNumber = phoneNumberUtil.parse(txtPhoneNumber.getText().toString(), country);
				if (!phoneNumberUtil.isValidNumber(phoneNumber)) {
					txtPhoneNumber.setError(Home.this.getText(R.string.home_invalidNumber));
					txtPhoneNumber.requestFocus();
					return;
				} else {
					// Phone number verified successfully.
					verifiedPhoneNumber = phoneNumberUtil.format(phoneNumber, PhoneNumberFormat.E164);
					registerPhoneWithServer();
				}
			} catch (NumberParseException e) {
				txtPhoneNumber.setText(numberBeforeEdit);
				txtPhoneNumber.setError(Home.this.getText(R.string.home_invalidNumber));
				txtPhoneNumber.requestFocus();
				return;
			}
		} else if ( v == errorButton) {
			errorMessageWindow.setVisibility(View.INVISIBLE);
			if (failBackStage == Stage.START) {
				performDeskPhoneInit();
			} else if (failBackStage == Stage.BEFORE_REGISTER) {
				handlePhoneNumberValidation();
			} else if (failBackStage == Stage.CHECK_REGISTRATION_STATUS) {
				checkDeviceActivationStatus();
			}
		}
	}

	private void registerPhoneWithServer() {
		statusMessage.setText(R.string.home_registeringphone);
		phoneNoView.setVisibility(View.INVISIBLE);

		Runnable phoneRegistrar = new Runnable() {
			@Override
			public void run() {
				try {
					DeskPhoneAccountService.registerAcount(
							Home.this, email, authenticated, verifiedPhoneNumber);

					checkDeviceActivationStatus();
				} catch(InvalidCDMCodeException cdex) {
					showErrorDialog();
					failBackStage = Stage.BEFORE_REGISTER;
					// Re-register
					dm.reRegister();
				} catch (IOException iox) {
					showErrorDialog();
					failBackStage = Stage.BEFORE_REGISTER;
				} catch (Exception e) {
					ErrorHandler.handleUiError(Home.this, e);
				}
			}
		};
		Thread t = new Thread(phoneRegistrar);
		t.start();
	}

	private void checkDeviceActivationStatus() {
		// If the device is activated, show activation message. Else,
		// take the user to main app.

		Runnable activationChecker = new Runnable() {
			@Override
			public void run() {
				try {
					// Check C2DM registration. If its not present we cannot move forward.
					String c2dmHandler = dm.getC2dmHandler();
					int RETRY_COUNT = 5;
					long interval = 1000;
					while ((c2dmHandler == null || c2dmHandler.trim().length() == 0) && RETRY_COUNT > 0) {
						dm.registerIfNew();
						Thread.sleep(interval);
						RETRY_COUNT--;
						interval *= 2;
						c2dmHandler = dm.getC2dmHandler();
					}

					// If it's still not available, show error.
					if (c2dmHandler == null || c2dmHandler.trim().length() == 0) {
						failBackStage = Stage.BEFORE_REGISTER;
						showErrorDialog();
						return;
					}

					boolean activated = DeskPhoneAccountService.isActivated(Home.this);
					if (activated) {
						TelephonyService.reScheduleAllCalls(Home.this);

						ContactsHelper.startSync(Home.this);

						Intent intent = new Intent(Home.this, LandingPageActivity.class);
						GCMIntentService.removeAccountStatusChangeListener(Home.this);
						startActivity(intent);
					} else {
						failBackStage = Stage.CHECK_REGISTRATION_STATUS;
						getUiHandler().post(new Runnable() {
							@Override
							public void run() {
								activationRequiredView.setVisibility(View.VISIBLE);
							}
						});
					}
				} catch (IOException e) {
					failBackStage = Stage.CHECK_REGISTRATION_STATUS;
					showErrorDialog();
				} catch (Exception e) {
					ErrorHandler.handleUiError(Home.this, e);
				}
			}
		};
		new Thread(activationChecker).start();
		

	}

	private void handlePhoneNumberValidation() {
		getUiHandler().post(new Runnable() {
			@Override
			public void run() {
				PhoneNumber phoneNumber;
				try {
					phoneNumber = phoneNumberUtil.parse(dm.getPhoneNumber(), country);
					txtPhoneNumber.setText(phoneNumberUtil.format(phoneNumber, PhoneNumberFormat.NATIONAL));
					numberBeforeEdit = txtPhoneNumber.getText().toString();
				} catch (NumberParseException e) {
					txtPhoneNumber.setText("");
				}
				phoneNoView.setVisibility(View.VISIBLE);
			}
		});
		
	}

	private void showErrorDialog() {
		getUiHandler().post(new Runnable() {
			@Override
			public void run() {
				errorMessageWindow.setVisibility(View.VISIBLE);
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 10001) {
			authenticated = (data.getExtras().getString("authtoken") != null);
		}
	}

	enum Stage {
		START,
		BEFORE_REGISTER,
		CHECK_REGISTRATION_STATUS
	}

	@Override
	public void onAccountStatusChanged(String email, boolean isActive) {
		if (failBackStage == Stage.CHECK_REGISTRATION_STATUS) {
			checkDeviceActivationStatus();
		}
	}
}
