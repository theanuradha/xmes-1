package com.lahiru.xmes.data;

import java.util.HashSet;
import java.util.Set;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class CallRequestDataHandler extends SQLiteOpenHelper {
	private static CallRequestDataHandler instance;
	private static Set<CallRequestStatusChangeListener> statusChangeListeners;

	static {
		statusChangeListeners = new HashSet<CallRequestDataHandler.CallRequestStatusChangeListener>();
	}
	
	private static final String DATABASE_NAME = "DeskPhone";
	private static final int DATABASE_VERSION = 1;
	private static final String TABLE_CALL_REQUEST = "CALL_REQUEST";
	private static final String CALL_REQUEST_KEY_ID = "_id";
	private static final String CALL_REQEST_KEY_NUMBER = "number";
	private static final String CALL_REQEST_KEY_NOTE = "note";
	private static final String CALL_REQEST_KEY_TIME = "time";
	private static final String CALL_REQEST_KEY_TYPE = "type";

	private static final String COUNT_ACTIVE_CALL_REQUESTS = "SELECT count(*) FROM "
			+ TABLE_CALL_REQUEST
			+ " WHERE "
			+ CALL_REQEST_KEY_TYPE
			+ "='"
			+ CallRequest.TYPE_FRESH + "'";

	private static final String COUNT_SCHEDULED_CALL_REQUESTS = "SELECT count(*) FROM "
			+ TABLE_CALL_REQUEST
			+ " WHERE "
			+ CALL_REQEST_KEY_TYPE
			+ "='"
			+ CallRequest.TYPE_SCHEDULED + "'";

	private static final String SELECT_NEXT_ACTIVE_IN_QUEUE = "SELECT * FROM "
			+ TABLE_CALL_REQUEST + " WHERE " + CALL_REQEST_KEY_TYPE + "= '"
			+ CallRequest.TYPE_FRESH + "' ORDER BY " + CALL_REQEST_KEY_TIME;

	private static final String SELECT_BY_ID = "SELECT * FROM "
			+ TABLE_CALL_REQUEST + " WHERE " + CALL_REQUEST_KEY_ID + "= ?";

	private static final String SELECT_BY_TYPE = "SELECT * FROM "
			+ TABLE_CALL_REQUEST + " WHERE " + CALL_REQEST_KEY_TYPE + "= ?";
	
	public static CallRequestDataHandler INSTANCE(Context context) {
		if (instance == null) {
			instance = new CallRequestDataHandler(context);
		}

		return instance;
	}

	public CallRequestDataHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_CALL_REQUEST + "("
        + CALL_REQUEST_KEY_ID + " INTEGER PRIMARY KEY,"
				+ CALL_REQEST_KEY_NUMBER + " TEXT,"
				+ CALL_REQEST_KEY_NOTE + " TEXT,"
				+ CALL_REQEST_KEY_TIME + " INTEGER,"
        + CALL_REQEST_KEY_TYPE + " TEXT" + ")";

		db.execSQL(CREATE_CONTACTS_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Right now we are not doing anything on upgrade.
	}

	public void addCallRequest(CallRequest cr) {
		ContentValues content = fromCallRequest(cr);
		SQLiteDatabase db = this.getWritableDatabase();
		db.insert(TABLE_CALL_REQUEST, null, content);
		db.close();
		notifyStatusUpdated();
	}

	public CallRequest findCallRequest(String id) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(SELECT_BY_ID, new String[]{id});

		if (cursor.moveToFirst()) {
			CallRequest request = toCallRequest(cursor);
			db.close();
			return request;
		} else {
			db.close();
			return null;
		}
		
	}

	public Cursor findCallRequestsByType(String type) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(SELECT_BY_TYPE, new String[]{type});

		return cursor;
	}

	public CallRequest readNextActiveCallRequest() {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(SELECT_NEXT_ACTIVE_IN_QUEUE, null);

		if (cursor.moveToFirst()) {
			CallRequest request = toCallRequest(cursor);
			db.close();
			return request;
		} else {
			db.close();
			return null;
		}
		
	}

	public void deleteCallRequest(CallRequest cr) {
		SQLiteDatabase db = this.getReadableDatabase();
		db.delete(TABLE_CALL_REQUEST, CALL_REQUEST_KEY_ID + "=?", new String[] {cr.get_id()});
		notifyStatusUpdated();
		db.close();
	}

	public void deleteAllActiveCallRequests() {
		SQLiteDatabase db = this.getReadableDatabase();
		
		ContentValues c = new ContentValues();
		c.put(CALL_REQEST_KEY_TYPE, CallRequest.TYPE_DELETED);
		db.update(TABLE_CALL_REQUEST, c , CALL_REQEST_KEY_TYPE + "=?",
				new String[] { CallRequest.TYPE_FRESH });
		db.close();
		notifyStatusUpdated();
	}

	public int numActiveCalls() {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(COUNT_ACTIVE_CALL_REQUESTS , null);
		cursor.moveToFirst();
		int count = cursor.getInt(0);
		return count;
	}

	public int numScheduledCalls() {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(COUNT_SCHEDULED_CALL_REQUESTS , null);
		cursor.moveToFirst();
		int count = cursor.getInt(0);
		return count;
	}

	public void update(CallRequest cr) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues c = fromCallRequest(cr);
		db.update(TABLE_CALL_REQUEST, c, CALL_REQUEST_KEY_ID + "= ?" , new String[]{cr.get_id()});
		db.close();
		notifyStatusUpdated();
	}

	public ContentValues fromCallRequest(CallRequest cr) {
		ContentValues content = new ContentValues();
		content.put(CALL_REQEST_KEY_NOTE, cr.getNote());
		content.put(CALL_REQEST_KEY_NUMBER, cr.getNumber());
		content.put(CALL_REQEST_KEY_TIME, cr.getTime());
		content.put(CALL_REQEST_KEY_TYPE, cr.getType());

		return content;
	}

	public CallRequest toCallRequest(Cursor c) {
		CallRequest cr = new CallRequest(
				c.getString(1),
				c.getString(2),
				c.getLong(3),
				c.getString(4));
		cr.set_id(c.getString(0));

		return cr;
	}

	public static interface CallRequestStatusChangeListener {
		/**
		 * Only notifies that it was updated. No need to be specific.
		 */
		void statusUpdated();
	}

	public static boolean addStatusChangeListener(CallRequestStatusChangeListener object) {
		return statusChangeListeners.add(object);
	}

	public static boolean removeStatusChangeListener(CallRequestStatusChangeListener object) {
		return statusChangeListeners.remove(object);
	}

	private void notifyStatusUpdated() {
		for (CallRequestStatusChangeListener l : statusChangeListeners) {
			l.statusUpdated();
		}
	}
}
