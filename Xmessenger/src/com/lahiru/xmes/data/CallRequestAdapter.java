package com.lahiru.xmes.data;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lahiru.xmes.MainActivity;
import com.lahiru.xmes.R;
import com.lahiru.xmes.service.TelephonyService;

@SuppressLint("SimpleDateFormat")
public class CallRequestAdapter extends CursorAdapter {
	private View currentlySelected;
	private SimpleDateFormat timeFormat;
	private SimpleDateFormat dateFormat;

	public CallRequestAdapter(Context context, Cursor c) {
		super(context, c);
		currentlySelected = null;
		timeFormat = new SimpleDateFormat("h:mm a");
		dateFormat = new SimpleDateFormat("EEE, MMM d");
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		CallRequest request = CallRequestDataHandler.INSTANCE(context).toCallRequest(cursor);
		ViewHolder vh = (ViewHolder) view.getTag();
		populateView(context, request, vh );
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		CallRequest request = CallRequestDataHandler.INSTANCE(context).toCallRequest(cursor);
		View tx = buildView(context, request, parent);
		return tx;
	}

	private View buildView(Context context, CallRequest request, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		ViewHolder vh = new ViewHolder();
    View rowView = inflater.inflate(R.layout.view_call_item, parent, false);
    vh.txtCallItemPhoneNumber = (TextView) rowView.findViewById(R.id.txtCallItemPhoneNumber);
    vh.txtCallItemTime = (TextView) rowView.findViewById(R.id.txtCallItemTime);
    vh.txtCallItemNote = (TextView) rowView.findViewById(R.id.txtCallItemNote);
    vh.lvButtonBar = (LinearLayout) rowView.findViewById(R.id.lvButtonBar);
    vh.btnLItemCall = (ImageView) rowView.findViewById(R.id.btnLItemCall);
    vh.btnLItemSnooze = (ImageView) rowView.findViewById(R.id.btnLItemSnooze);
    vh.btnCallCancelReminder = (ImageView) rowView.findViewById(R.id.btnLItemDelete);
    populateView(context, request, vh);
    
    rowView.setOnClickListener(new ItemOnClick());
    rowView.setTag(vh);
    return rowView;
	}

	protected void populateView(Context context, CallRequest request, final ViewHolder vh) {
		vh.lvButtonBar.setVisibility(View.INVISIBLE);
    vh.txtCallItemPhoneNumber.setText(request.getNumber());
    vh.txtCallItemTime.setText(getTimeString(request.getTime()));
    vh.txtCallItemNote.setText(request.getNote());
 
    vh.btnLItemCall.setOnClickListener(new OnCallInitiate());
    vh.btnLItemSnooze.setOnClickListener(new OnCallResechedule());
    vh.btnCallCancelReminder.setOnClickListener(new OnDeleteCall());
    vh.callRequest = request;
    vh.context = context;
	}

	private CharSequence getTimeString(long time) {
		Calendar calendar = Calendar.getInstance();
		Calendar today = Calendar.getInstance();
		calendar.setTimeInMillis(time);

		String timeString = "";
		if (calendar.get(Calendar.MONTH) == today.get(Calendar.MONTH) 
				&& calendar.get(Calendar.DAY_OF_MONTH) == today.get(Calendar.DAY_OF_MONTH)) {
			timeString = timeFormat.format(calendar.getTime());
		}else {
			timeString = dateFormat.format(calendar.getTime());
		}
		return timeString;
	}

	private final class OnDeleteCall implements
			View.OnClickListener {
		@Override
		public void onClick(View v) {
			ViewHolder vh = (ViewHolder) currentlySelected.getTag();
			MainActivity ma = (MainActivity) vh.context;
			CallRequestDataHandler.INSTANCE(ma).deleteCallRequest(vh.callRequest);
			ma.refresh();
		}
	}

	private final class OnCallInitiate implements
			View.OnClickListener {
		@Override
		public void onClick(View v) {
			ViewHolder vh = (ViewHolder) currentlySelected.getTag();

			TelephonyService.makePhoneCall(vh.context, vh.callRequest);
			vh.callRequest.setType(CallRequest.TYPE_FINISHED);
			CallRequestDataHandler.INSTANCE(vh.context).update(vh.callRequest);
			MainActivity ma = (MainActivity) vh.context;
			ma.refresh();
			
		}
	}

	private final class OnCallResechedule implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			ViewHolder vh = (ViewHolder) currentlySelected.getTag();

			MainActivity ma = (MainActivity) vh.context;
			ma.showReScheduleDialog(vh.callRequest);
		}
	}

	private final class ItemOnClick implements
			View.OnClickListener {
		@Override
		public void onClick(View v) {
			if (currentlySelected != null) {
				currentlySelected.setBackgroundResource(R.drawable.list_selector);
				LinearLayout lvButtonBar = (LinearLayout) currentlySelected.findViewById(R.id.lvButtonBar);
				lvButtonBar.setVisibility(View.INVISIBLE);
			}

			currentlySelected = v;
			LinearLayout lvButtonBar = (LinearLayout) currentlySelected.findViewById(R.id.lvButtonBar);
			lvButtonBar.setVisibility(View.VISIBLE);
			v.setBackgroundResource(R.drawable.list_selected_row);
		}
	}

	public static class ViewHolder {
		TextView txtCallItemPhoneNumber;
    TextView txtCallItemTime;
    TextView txtCallItemNote;
    LinearLayout lvButtonBar;
    ImageView btnLItemCall;
    ImageView btnLItemSnooze;
    ImageView btnCallCancelReminder;
    CallRequest callRequest;
    Context context;
	}
}
