package com.lahiru.xmes;

import com.google.ads.AdView;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class LandingPageActivity extends AbstractDeskphoneActivity implements
		OnClickListener {
	private RelativeLayout lvPopup;
	private TextView landingSiteUrl;
	private AdView adViewLanding;
	private Button btnCallRemindersLanding;
	private Button btnSettingsLanding;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.landingpage);
		init();
	}

	private void init() {
		lvPopup = (RelativeLayout) findViewById(R.id.lvPopupLanding);
		landingSiteUrl = (TextView) findViewById(R.id.landingSiteUrl);
		adViewLanding = (AdView) findViewById(R.id.adViewLanding);
		btnCallRemindersLanding = (Button) findViewById(R.id.btnCallRemindersLanding);
		btnSettingsLanding = (Button) findViewById(R.id.btnSettingsLanding);

		landingSiteUrl.setMovementMethod(LinkMovementMethod.getInstance());
		btnCallRemindersLanding.setOnClickListener(this);
		btnSettingsLanding.setOnClickListener(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected View getDectivatedPopup() {
		return lvPopup;
	}

	@Override
	protected AdView getAdView() {
		return adViewLanding;
	}

	@Override
	public void onClick(View v) {
		if (v == btnCallRemindersLanding) {
			Intent intent = new Intent(this, MainActivity.class);
			startActivity(intent );
		} else if (v == btnSettingsLanding) {
			Intent intent = new Intent(this, SettingsPageActivity.class);
			startActivity(intent );
		}
	}
}
