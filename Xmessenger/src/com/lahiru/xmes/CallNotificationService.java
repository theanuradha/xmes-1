package com.lahiru.xmes;

import com.lahiru.xmes.data.CallRequest;
import com.lahiru.xmes.data.CallRequestDataHandler;
import com.lahiru.xmes.service.TelephonyService;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

/**
 * Schedule and invoke phone call requests.
 * @author lahiruw
 *
 */
public class CallNotificationService extends Service {

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	 @Override
   public void onStart(Intent intent, int startId) {
		try {
			String crId = intent.getExtras().getString("cr");
			CallRequest callRequest = CallRequestDataHandler.INSTANCE(this).findCallRequest(crId);

			if (callRequest != null && callRequest.getType().equals(CallRequest.TYPE_SCHEDULED)) {
				TelephonyService.processCall(this, callRequest);
			}
		} catch (Exception e) {
			ErrorHandler.handleNonUiError(e);
		}
  }
}
