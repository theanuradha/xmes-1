package com.lahiru.xmes;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.ads.AdView;
import com.lahiru.xmes.service.DeskPhoneAccountService;
import com.lahiru.xmes.service.DeviceManager;

public class SettingsPageActivity extends AbstractDeskphoneActivity implements
		OnClickListener, OnCheckedChangeListener {

	private RelativeLayout lvPopup;
	private AdView adView;
	private CheckBox chkOnSms;
	private CheckBox chkOnMissedCall;
	private TextView lblGoogleAccountName;

	DeviceManager dm;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		try {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.settingspage);

			dm = DeviceManager.forContext(this);

			init();
		} catch (Exception e) {
			ErrorHandler.handleUiError(this, e);
		}
	}

	private void init() throws Exception {
		lvPopup = (RelativeLayout) findViewById(R.id.lvPopupSettings);
		adView = (AdView) findViewById(R.id.adViewLanding);
		chkOnSms = (CheckBox) findViewById(R.id.chkOnSms);
		chkOnMissedCall = (CheckBox) findViewById(R.id.chkOnMissedCall);
		lblGoogleAccountName = (TextView) findViewById(R.id.lblGoogleAccountName);

		chkOnSms.setOnCheckedChangeListener(this);
		chkOnMissedCall.setOnCheckedChangeListener(this);

		chkOnSms.setChecked(dm.isSmsNotificationEnabled());
		chkOnMissedCall.setChecked(dm.isCallNotificationEnabled());
		lblGoogleAccountName.setText(DeskPhoneAccountService.getActiveAccounts(this).get(0));
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected View getDectivatedPopup() {
		return lvPopup;
	}

	@Override
	protected AdView getAdView() {
		return adView;
	}

	@Override
	public void onClick(View v) {
	}

	@Override
	public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
		if (arg0 == chkOnMissedCall) {
			dm.setCallNotificationEnabled(arg1);
		} else if (arg0 == chkOnSms) {
			dm.setSmsNotificationEnabled(arg1);
		}
	}
}
