package com.lahiru.xmes.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract.CommonDataKinds;
import android.provider.ContactsContract.Contacts;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.lahiru.xmes.ErrorHandler;
import com.lahiru.xmes.data.ContactsDataHandler;
import com.lahiru.xmes.data.DPContact;
import com.lahiru.xmes.data.DPContact.ContactInfo;
import com.lahiru.xmes.data.DPContact.ContactType;

@SuppressLint("DefaultLocale")
public class ContactsHelper {
	public static final String CONTACT_SYNC_STATUS_SYNC_COMPLETE = "complete";
	public static final String CONTACT_SYNC_STATUS_NEED_SERVER_SYNC = "serversyncneeded";
	private static final int BATCH_SIZE = 100;
	private static final int RETRY_LIMIT = 5;

	private static ContactsHelper instance = new ContactsHelper();
	private String country;

	public static ContactsHelper i() {
		return instance;
	}

	@SuppressLint("InlinedApi")
	public boolean syncAllContacts(Context context) {
		boolean updated = false;
		ContentResolver contentResolver = context.getContentResolver();

		DPContact current = null;

		Cursor numCursor = contentResolver.query(
				CommonDataKinds.Phone.CONTENT_URI, null, null, null, CommonDataKinds.Phone.CONTACT_ID);
		try {

			while (numCursor.moveToNext()) {
				try {
					String phoneNumber = numCursor.getString(numCursor.getColumnIndex(CommonDataKinds.Phone.NUMBER));
					String name = numCursor.getString(numCursor.getColumnIndex(CommonDataKinds.Phone.DISPLAY_NAME));
					int phoneType = numCursor.getInt(numCursor.getColumnIndex(CommonDataKinds.Phone.TYPE));
					int contactID = numCursor.getInt(numCursor.getColumnIndex(CommonDataKinds.Phone.CONTACT_ID));
					int version = numCursor.getInt(numCursor.getColumnIndex(CommonDataKinds.Phone.DATA_VERSION));
					int _id = numCursor.getInt(numCursor.getColumnIndex(Contacts._ID));
					phoneNumber = formatE164(phoneNumber, context);

					if (current != null && current.getId() != contactID) {
						// Save existing.
						updated = saveContact(context, current);
						current = null;
					}

					current = updateOrCreateContact(current, contentResolver,_id, phoneNumber,
							null, name, phoneType, contactID, version);
					// Thumbnail
					String thumbnailUri = getThumbnailId(numCursor);
					current.setPicture(getThumbnail(context, thumbnailUri));

				} catch (Exception e) {
					continue; // Ignore, theres nothing we can do here.
				}
			}

		} finally {
			numCursor.close();
		}
		return updated;
	}

	public String formatE164(String phoneNumber, Context ctx)
			throws NumberParseException {
		PhoneNumberUtil numberUtil = PhoneNumberUtil.getInstance();

		PhoneNumber parsedNumber = numberUtil.parse(phoneNumber, 
				getCountry(ctx));
		phoneNumber = numberUtil.format(parsedNumber, PhoneNumberFormat.E164);
		return phoneNumber;
	}

	public boolean udateContactsToServer(Context context) {
		try {
			ContactsDataHandler contactsDao = ContactsDataHandler.i(context);

			List<DPContact> dirtyContacts = contactsDao.getDirtyContacts();

			int remaining = dirtyContacts.size();
			int currentIndex = 0;

			List<DPContact> currentSet = new ArrayList<DPContact>();
			List<NameValuePair> serverData = new ArrayList<NameValuePair>();
			while (remaining > 0) {
				currentSet.clear();
				serverData.clear();

				JSONArray contactsArray = new JSONArray();

				for (int i=0; i <= BATCH_SIZE && remaining-- > 0; i++) {
					DPContact contact = dirtyContacts.get(currentIndex++);
					contactsArray.put(i, contact.toJson());
					currentSet.add(contact);
				}

				serverData.add(new BasicNameValuePair(
							"emails", DeskPhoneAccountService.getAccountsJSON(context)));
				serverData.add(new BasicNameValuePair("contactData", contactsArray.toString()));
				DeviceManager dm = DeviceManager.forContext(context);
				serverData.add(new BasicNameValuePair("deviceId", dm.getRegistrationId()));
				serverData.add(new BasicNameValuePair("country", dm.getCountry()));

				int retryCount = 0;
				// Try to update.
				boolean success = false;
				while (retryCount  < RETRY_LIMIT) {
					try {
						TalkToServer.postToServer(serverData, TalkToServer.POST_CONTACT_DETAILS);

						// Update was success. Mark them.
						contactsDao.markNotDirty(currentSet);
						success = true;
						break;
					} catch(IOException iox) {
						retryCount++;
					}
				}
				if (!success) {
					return false;
				}
			}
		} catch (Exception e) {
			ErrorHandler.handleNonUiError(e);
			return false;
		}
		
		return true;
	}

	@SuppressLint("InlinedApi")
	private String getThumbnailId(Cursor numCursor) {
		int thumbnailColumn;
		if (DeviceManager.isPostHoneycomb()) {
			thumbnailColumn = numCursor.getColumnIndex(Contacts.PHOTO_THUMBNAIL_URI);
		} else {
			thumbnailColumn = numCursor.getColumnIndex(Contacts._ID);
		}
		String thumbnailUri = numCursor.getString(thumbnailColumn);
		return thumbnailUri;
	}

	protected DPContact updateOrCreateContact(DPContact current, ContentResolver contentResolver,
			int id, String phoneNumber, byte[] photo, String name, int phoneType,
			int contactID, int version) {
		if (current == null) {
			current = new DPContact(contactID, name, photo);
		}
		current.getContactInfo().add(
				new ContactInfo(id, ContactType.parse(phoneType), phoneNumber, version));

		return current;
	}

	private boolean saveContact(Context context, DPContact current) {
		ContactsDataHandler contactsDao = ContactsDataHandler.i(context);
		return contactsDao.storeContact(current);
	}

	private byte[] getThumbnail(Context context, String photoId) {
		if (photoId == null) {
			return null;
		}

		Uri thumbUri;
		AssetFileDescriptor afd = null;
		try {
			if (DeviceManager.isPostHoneycomb()) {
				thumbUri  = Uri.parse(photoId);
			} else {
				final Uri contactsUri = Uri.withAppendedPath(Contacts.CONTENT_URI, photoId);
				thumbUri = Uri.withAppendedPath(
						contactsUri, android.provider.ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
			}

			afd = context.getContentResolver()
					.openAssetFileDescriptor(thumbUri, "r");

			if (afd != null) {
				Bitmap bitmap = BitmapFactory.decodeFileDescriptor(afd.getFileDescriptor(), null, null);
				ByteArrayOutputStream imageInfo = new ByteArrayOutputStream();
				bitmap.compress(CompressFormat.JPEG, 10, imageInfo);

				return imageInfo.toByteArray();
			}
		} catch (Exception e) {
			// Not finding contact picture is trivial. Ignore.
			return null;
		} finally {
			if (afd != null) {
				try {
					afd.close();
				} catch (IOException e) {}
			}
		}
		return null;
	}

	public static void startSync(Context context) {
		Intent contactSyncIntent = new Intent(context, ContactSyncService.class);
		context.startService(contactSyncIntent);
		//new ContactSyncService(context).onHandleIntent(null);
	}

	public String getCountry(Context ctx) {
		if (country == null) {
			try {
				country = DeviceManager.forContext(ctx).getCountry().toUpperCase(Locale.US);
			} catch (Exception e) {
				country = "US";
			}
		}

		return country;
	}
}
