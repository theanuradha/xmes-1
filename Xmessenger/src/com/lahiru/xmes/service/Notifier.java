package com.lahiru.xmes.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.os.Vibrator;
import android.provider.Settings;

import com.lahiru.xmes.R;

public class Notifier {
	public static final int NEW_CALL_REQUEST_NOTIFICATION = 10001;
	public static final int SCHEDULED_CALL_REQUEST_NOTIFICATION = 10002;
	public static final int ANNOUNCEMENT = 10003;

	public static void doVibrateAndRing(Context ctx) {
		AudioManager am = (AudioManager)ctx.getSystemService(Context.AUDIO_SERVICE);

		switch (am.getRingerMode()) {
			case AudioManager.RINGER_MODE_NORMAL:
				Ringtone rt = RingtoneManager.getRingtone(ctx, Settings.System.DEFAULT_NOTIFICATION_URI);
				if (rt != null) rt.play();
			case AudioManager.RINGER_MODE_VIBRATE:
				Vibrator vibrator = (Vibrator) ctx.getSystemService(Context.VIBRATOR_SERVICE);
				vibrator.vibrate(300);
				break;
			default: break;
		}
	}

	public static void createNotification(
			Context ctx, String title, String tickerttext, Intent intent, int id) {
		int icon = R.drawable.icon_large;
		Intent notificationIntent = intent;
		PendingIntent contentIntent = PendingIntent.getActivity(ctx, 0,
        notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

		NotificationManager mNotificationManager = 
				(NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = new Notification(icon, tickerttext, System.currentTimeMillis());
		notification.setLatestEventInfo(ctx, title, tickerttext, contentIntent);
		notification.ledARGB = 0x0066ff;
		notification.flags |= Notification.FLAG_SHOW_LIGHTS;
		mNotificationManager.notify(id, notification);
	}

	public static void clear(Context ctx, int notificationId) {
		NotificationManager nm = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
		nm.cancel(notificationId);
	}
}
