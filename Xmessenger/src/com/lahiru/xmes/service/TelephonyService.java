package com.lahiru.xmes.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.telephony.SmsManager;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.lahiru.xmes.CallNotificationService;
import com.lahiru.xmes.CallRequestNotifyActivity;
import com.lahiru.xmes.ErrorHandler;
import com.lahiru.xmes.Home;
import com.lahiru.xmes.R;
import com.lahiru.xmes.data.CallRequest;
import com.lahiru.xmes.data.CallRequestDataHandler;
import com.lahiru.xmes.exception.InvalidCDMCodeException;
import com.lahiru.xmes.util.Utilities;
import com.lahiru.xmes.workers.ServerMessageSender;

public class TelephonyService {
	/**
	 * If Phone is locked display a notification.
	 * If currently on the phone wait until the phone call is over.
	 * Else show a confirmation dialog asking user to proceed.
	 * @throws Exception 
	 */
	public static void publishPhoneCallRequest(Context cxt, CallRequest req) throws Exception {
		req.setNumber(Utilities.santizePhoneNumber(req.getNumber()));
		CallQueue.push(req, cxt);
		if (!CallRequestNotifyActivity.IS_DIALOG_DISPLAYED) {
			processNextCall(cxt);
		}
	}

	@SuppressLint("DefaultLocale")
	public static void sendSms(Context cxt, String number, String message) throws Exception {
		number = Utilities.santizePhoneNumber(number);
		PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
		String country = DeviceManager.forContext(cxt).getCountry();
		country = (country != null) ? country.toUpperCase() : "US";
		PhoneNumber phoneNumber = phoneNumberUtil.parse(number, country);

		ArrayList<String> messages = new ArrayList<String>();
		if (message.length() < 160) {
			messages.add(message);
		} else {
			int current = 0;
			while (current < message.length()-1) {
				int end = Math.min(message.length() - (current + 1), current + 140);
				String m = message.substring(current, current + end);
				messages.add(m);
				current = current + end;
			}
		}

		SmsManager smsManager = SmsManager.getDefault();
    smsManager.sendMultipartTextMessage(
    		phoneNumberUtil.format(phoneNumber, PhoneNumberFormat.NATIONAL),
    		null,
    		messages,
    		null,
    		null);
    ContentHelper.saveSentMessageToConversations(
    		cxt, phoneNumberUtil.format(phoneNumber, PhoneNumberFormat.E164), message);
	}

	public static void reScheduleAllCalls(Context cxt) throws Exception {
		CallRequestDataHandler dataHandler = CallRequestDataHandler.INSTANCE(cxt);
		Cursor requestsByType = dataHandler.findCallRequestsByType(CallRequest.TYPE_SCHEDULED);
		if (requestsByType.moveToFirst()) {
			do {
				CallRequest s = dataHandler.toCallRequest(requestsByType);
	
				Intent notifierService = new Intent(cxt, CallNotificationService.class);
				notifierService.putExtra("cr", s.get_id());
				PendingIntent pintent = PendingIntent.getService(
						cxt, Integer.parseInt(s.get_id()), notifierService, PendingIntent.FLAG_ONE_SHOT);
				AlarmManager alarm = (AlarmManager)cxt.getSystemService(Context.ALARM_SERVICE);
				alarm.set(AlarmManager.RTC_WAKEUP, s.getTime(), pintent);
			} while(requestsByType.moveToNext());
		}

		TelephonyService.updateScheduledCallNotification(cxt);
		
	}

	public static void updatePendingCallNotification(Context cxt) {
		int queueSize = CallQueue.queueSize(cxt);

		CallRequest request = CallQueue.peek(cxt);
		String msg = "New call request to " + request.getNumber();
		if (queueSize > 1) {
			msg += " and " + (queueSize - 1) + "others";
		}

		Notifier.createNotification(
        cxt,
        cxt.getString(R.string.app_name),
        msg,
        new Intent(cxt, CallRequestNotifyActivity.class),
        Notifier.NEW_CALL_REQUEST_NOTIFICATION);
		Notifier.doVibrateAndRing(cxt);
	}

	public static void updateScheduledCallNotification(Context cxt) {
		int queueSize = CallRequestDataHandler.INSTANCE(cxt).numScheduledCalls();

		if (queueSize <= 0) {
			Notifier.clear(cxt, Notifier.SCHEDULED_CALL_REQUEST_NOTIFICATION);
			return;
		}

		
		String msg = "You have scheduled calls. Click here to call now";

		Notifier.createNotification(
        cxt,
        cxt.getString(R.string.app_name),
        msg,
        new Intent(cxt, Home.class),
        Notifier.SCHEDULED_CALL_REQUEST_NOTIFICATION);
		Notifier.doVibrateAndRing(cxt);
	}

	public static void makePhoneCall(Context cxt, CallRequest cr) {
		Intent in = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+ cr.getNumber()));
		cxt.startActivity(in);
	}

	public static void processNextCall(final Context c) {
		if (!CallQueue.isQueueEmpty(c)) {
			updatePendingCallNotification(c);
			// Show the dialog asking confirmation to start call.
			Intent in = new Intent(c, CallRequestNotifyActivity.class);
			in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			c.startActivity(in);
		} else {
			Notifier.clear(c, Notifier.NEW_CALL_REQUEST_NOTIFICATION);
			return;
		}
	}

	public static void processCall(final Context c, CallRequest cr) throws Exception {
		Intent in = new Intent(c, CallRequestNotifyActivity.class);
		in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		in.putExtra(CallRequestNotifyActivity.SCHEDULED_CALL_REQUEST_ID, cr.get_id());
		Notifier.doVibrateAndRing(c);
		c.startActivity(in);
	}

	public static void onMessageReceived(final Context c, final String number, final String messageBody) {
		// Use a new thread.
		final ServerMessageSender smsNotifier = new ServerMessageSender() {
			@Override
			public void run() {
				try {
					NameValuePair byNumber = ContentHelper.getContactDisplayNameByNumber(c, number);
					String senderContact = byNumber.getName();
					String senderId = byNumber.getValue();
					List<String> emails = DeskPhoneAccountService.getActiveAccounts(c);
					JSONObject requestParams = new JSONObject();
					requestParams.put("emails", new JSONArray(emails));
					requestParams.put("time", System.currentTimeMillis());
					requestParams.put("fromPhone", DeviceManager.forContext(c).getDeviceName());
					requestParams.put("contactName", senderContact);
					requestParams.put("no", number);
					requestParams.put("messageContents", messageBody);
					requestParams.put("phoneId", DeviceManager.forContext(c).getRegistrationId());
					requestParams.put("contactId", senderId != "?" ? Long.parseLong(senderId) : -1);

					TalkToServer.onSms(requestParams.toString());
				} catch (IOException ex) {
					retry();
				} catch (InvalidCDMCodeException cdex){
					DeviceManager.forContext(c).reRegister();
				} catch (Exception ex) {
					ErrorHandler.handleNonUiError(ex);
				}
			}
		};

		new Thread(smsNotifier).start();
	}

	public static void onMissedCall(final Context c, final String number, final String callerName, final String callerContactId) {
		// Use a new thread.
		final ServerMessageSender missedCallNotifier = new ServerMessageSender() {

			@Override
			public void run() {
				try {
					List<String> emails = DeskPhoneAccountService.getActiveAccounts(c);
					JSONObject requestParams = new JSONObject();
					requestParams.put("no", number);
					requestParams.put("emails", new JSONArray(emails));
					requestParams.put("time", System.currentTimeMillis());
					requestParams.put("fromPhone", DeviceManager.forContext(c).getDeviceName());
					requestParams.put("contactName", callerName);
					requestParams.put("phoneId", DeviceManager.forContext(c).getRegistrationId());
					requestParams.put("contactId", callerContactId != "?" ? Long.parseLong(callerContactId) : -1);

					TalkToServer.missedCall(requestParams.toString());
				} catch (IOException ex) {
					retry();
				} catch(InvalidCDMCodeException c2ex) {
					DeviceManager.forContext(c).reRegister();
				} catch (Exception ex) {
					ErrorHandler.handleNonUiError(ex);
				}
			}
		};

		new Thread(missedCallNotifier).start();
	}
}
