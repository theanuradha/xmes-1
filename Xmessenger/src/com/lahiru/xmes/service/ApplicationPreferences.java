package com.lahiru.xmes.service;

import java.util.LinkedHashMap;
import java.util.Map;

import android.content.Context;
import android.content.SharedPreferences;

import com.lahiru.xmes.R;

public class ApplicationPreferences {
	private static Map<String, Long> remindOptions;
	private static long MINUTES = 1000 * 60;
	private static long HOURS = MINUTES * 60;

	static {
		remindOptions = new LinkedHashMap<String, Long>();
		remindOptions.put("5 Minutes", 5 * MINUTES);
		remindOptions.put("15 Minutes", 15 * MINUTES);
		remindOptions.put("30 Minutes", 30 * MINUTES);
		remindOptions.put("1 Hour", 1 * HOURS);
		remindOptions.put("2 Hours", 2 * HOURS);
		remindOptions.put("4 Hours", 4 * HOURS);
	}

	public static String getPreference(Context context, String pref) {
		SharedPreferences preferences = context.getSharedPreferences(
				context.getString(R.string.xmes_accountpref), 0);
		String val = preferences.getString(pref, "");
		return val;
	}

	public static void savePreference(String which, String value, Context context) {
		SharedPreferences preferences = context.getSharedPreferences(
				context.getString(R.string.xmes_accountpref), 0);
		preferences.edit().putString(which, value).commit();
	}

	public static Map<String, Long> getRemindOptions() {
		return remindOptions;
	}
}
