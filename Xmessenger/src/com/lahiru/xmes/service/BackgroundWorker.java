package com.lahiru.xmes.service;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class BackgroundWorker {
	private ScheduledExecutorService threadPool;
	private static BackgroundWorker instance;

	private BackgroundWorker() {
		threadPool = Executors.newScheduledThreadPool(5);
	}

	public void perform(Runnable work, long delay) {
		threadPool.schedule(work, delay, TimeUnit.SECONDS);
	}

	public static BackgroundWorker i() {
		if (instance == null) {
			instance = new BackgroundWorker();
		}

		return instance;
	}
}
