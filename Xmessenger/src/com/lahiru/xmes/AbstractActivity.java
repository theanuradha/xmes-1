package com.lahiru.xmes;

import com.google.analytics.tracking.android.EasyTracker;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import android.util.DisplayMetrics;

public class AbstractActivity extends Activity {
	private final Handler uiHandler = new Handler();
	private ProgressDialog progress;

	protected void showProgressDialog(final String msg) {
		getUiHandler().post(new Runnable() {
			@Override
			public void run() {
				progress = ProgressDialog.show(AbstractActivity.this, "Please wait...",
						msg);
			}
		});
	}

	protected void hideProgressDialog() {
		getUiHandler().post(new Runnable() {
			@Override
			public void run() {
				if (progress != null) {
					progress.hide();
				}
				progress = null;
			}
		});
	}

	public static float convertDpToPixel(float dp,Context context){
    Resources resources = context.getResources();
    DisplayMetrics metrics = resources.getDisplayMetrics();
    float px = dp * (metrics.densityDpi/160f);
    return px;
}

	public Handler getUiHandler() {
		return uiHandler;
	}

	@Override
	protected void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this);
	}

	@Override
	protected void onStop() {
		super.onStop();
		EasyTracker.getInstance().activityStop(this);
	}
}
