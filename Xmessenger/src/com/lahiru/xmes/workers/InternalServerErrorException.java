package com.lahiru.xmes.workers;

import java.io.IOException;

@SuppressWarnings("serial")
public class InternalServerErrorException extends IOException {
	public InternalServerErrorException(String err) {
		super(err);
	}
}
