package com.lahiru.xmes;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Map;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.lahiru.xmes.GCMIntentService.AccountStatusChangeListener;
import com.lahiru.xmes.data.CallRequest;
import com.lahiru.xmes.data.CallRequestAdapter;
import com.lahiru.xmes.data.CallRequestDataHandler;
import com.lahiru.xmes.data.CallRequestDataHandler.CallRequestStatusChangeListener;
import com.lahiru.xmes.service.ApplicationPreferences;
import com.lahiru.xmes.service.DeskPhoneAccountService;
import com.lahiru.xmes.service.TelephonyService;

@SuppressLint("SimpleDateFormat")
public class MainActivity extends SchedulableCallActivity implements
		OnCheckedChangeListener, AccountStatusChangeListener,
		CallRequestStatusChangeListener {

	private ToggleButton tbtnPending;
	private ToggleButton tbtnFinished;
	private ToggleButton tbMainRemindIn;
	private ToggleButton tbMainRemindAt;
	private ToggleButton tbtnCanceled;
	private ListView lvCalls;
	private Spinner spHomeRemindTimes;
	private Button btnSetTime;
	private Button btnCallAcceptReminder;
	private Button btnCallCancelReminder;
	private TextView listViewEmptyMessage;

	private ToggleButton currentView;
	private TextView activationLink;

	private RelativeLayout lvPopup;
	private RelativeLayout activationRequiredViewMain;
	private LinearLayout mainCallResechedule;
	private AdView adview;

	private Map<String, Long> remindOptions;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		init();
		showPendingCalls();
	}

	private void init() {
		lvCalls = (ListView) findViewById(R.id.lvCalls);
		tbtnPending = (ToggleButton) findViewById(R.id.tbtnPending);
		tbtnFinished = (ToggleButton) findViewById(R.id.tbtnFinished);
		tbtnCanceled = (ToggleButton) findViewById(R.id.tbtnCanceled);
		activationLink = (TextView) findViewById(R.id.activationLinkMain);
		lvPopup = (RelativeLayout) findViewById(R.id.lvPopup);
		activationRequiredViewMain = (RelativeLayout) findViewById(R.id.activationRequiredViewMain);
		mainCallResechedule = (LinearLayout) findViewById(R.id.mainCallResechedule);
		spHomeRemindTimes = (Spinner) findViewById(R.id.spHomeRemindTimes);
		tbMainRemindIn = (ToggleButton) findViewById(R.id.tbMainRemindIn);
		tbMainRemindAt = (ToggleButton) findViewById(R.id.tbMainRemindAt);
		btnSetTime = (Button) findViewById(R.id.btnSetTime);
		btnCallAcceptReminder = (Button) findViewById(R.id.btnCallAcceptReminder);
		btnCallCancelReminder = (Button) findViewById(R.id.btnCallCancelReminder);
		listViewEmptyMessage = (TextView) findViewById(R.id.listViewEmptyMessage);
		adview = (AdView) findViewById(R.id.adViewCalls);

		remindOptions = ApplicationPreferences.getRemindOptions();
		ArrayAdapter<String> remindAdapter = new ArrayAdapter<String>(
				this, android.R.layout.simple_spinner_item, new ArrayList<String>(remindOptions.keySet()));
		remindAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		lvCalls.setEmptyView(listViewEmptyMessage);
		spHomeRemindTimes.setAdapter(remindAdapter);

		tbtnPending.setOnCheckedChangeListener(this);
		tbtnFinished.setOnCheckedChangeListener(this);
		tbtnCanceled.setOnCheckedChangeListener(this);
		currentView = tbtnPending;
		activationLink.setMovementMethod(LinkMovementMethod.getInstance());

		GCMIntentService.addAccountStatusChangeListener(this);
		currentView.setChecked(true);

		CallRequestDataHandler.addStatusChangeListener(this);
	}

	

	@Override
	protected void onPause() {
		super.onPause();
		CallRequestDataHandler.addStatusChangeListener(this);
		GCMIntentService.removeAccountStatusChangeListener(this);
	}
	@Override
	protected void onResume() {
		super.onResume();
		GCMIntentService.addAccountStatusChangeListener(this);
		CallRequestDataHandler.removeStatusChangeListener(this);

		// Load ad
		AdRequest adRequest = new AdRequest();
		LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		adRequest.setLocation(location);
		adview.loadAd(adRequest);
	}
	private void showPendingCalls() {
		Cursor c = CallRequestDataHandler.INSTANCE(this).findCallRequestsByType(
				CallRequest.TYPE_SCHEDULED);
		lvCalls.setAdapter(new CallRequestAdapter(this, c));
	}

	private void showFinishedCalls() {
		Cursor c = CallRequestDataHandler.INSTANCE(this).findCallRequestsByType(
				CallRequest.TYPE_FINISHED);
		lvCalls.setAdapter(new CallRequestAdapter(this, c));
	}

	private void showDeletedCalls() {
		Cursor c = CallRequestDataHandler.INSTANCE(this).findCallRequestsByType(
				CallRequest.TYPE_DELETED);
		lvCalls.setAdapter(new CallRequestAdapter(this, c));
	}	

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		// Clicking the current view shouldnt change anything.
		if (currentView == buttonView && !isChecked) {
			currentView.setChecked(false);
			return;
		} else if (currentView != buttonView) {
			currentView.setChecked(false);
			currentView = (ToggleButton) buttonView;
		}

		if (buttonView == tbtnCanceled) {
			showDeletedCalls();
			listViewEmptyMessage.setText(getString(R.string.calls_noitem_cancel));
		} else if (buttonView == tbtnFinished) {
			showFinishedCalls();
			listViewEmptyMessage.setText(getString(R.string.calls_noitem_finish));
		} else if (buttonView == tbtnPending) {
			showPendingCalls();
			listViewEmptyMessage.setText(getString(R.string.calls_noitem_pending));
		}
	}
	@Override
	public void onAccountStatusChanged(String email, boolean isActive) {
		Runnable activationStatusChecker = new Runnable() {
			@Override
			public void run() {
				try {
					boolean activated = DeskPhoneAccountService.isActivatedLocally(MainActivity.this);
					final int visibilityStatus = activated? View.INVISIBLE : View.VISIBLE;
					getUiHandler().post(new Runnable() {
						@Override
						public void run() {
							lvPopup.setVisibility(visibilityStatus);
							activationRequiredViewMain.setVisibility(visibilityStatus);
						}
					});
				} catch (Exception e) {
					ErrorHandler.handleUiError(MainActivity.this, e);
				}
			}
		};
		new Thread(activationStatusChecker).start();
	}

	public void showReScheduleDialog(CallRequest cr) {
		getUiHandler().post(new CallReScheduleDialog(cr));
	}

	@Override
	public void statusUpdated() {
		getUiHandler().post(new Runnable() {
			@Override
			public void run() {
				if (currentView == tbtnCanceled) {
					showDeletedCalls();
				} else if (currentView == tbtnFinished) {
					showFinishedCalls();
				} else if (currentView == tbtnPending) {
					showPendingCalls();
				}

				TelephonyService.updateScheduledCallNotification(MainActivity.this);
			}
		});
	}

	private final class CallReScheduleDialog implements Runnable {
		private CallRequest cr;
		public CallReScheduleDialog(CallRequest cr) {
			this.cr = cr;
		}

		protected void hideThisWindow() {
			lastSetTime = null;
			lvPopup.setVisibility(View.INVISIBLE);
			mainCallResechedule.setVisibility(View.INVISIBLE);
		}

		@Override
		public void run() {
			lvPopup.setVisibility(View.VISIBLE);
			mainCallResechedule.setVisibility(View.VISIBLE);

			if (System.currentTimeMillis() < cr.getTime()) {
				lastSetTime = GregorianCalendar.getInstance();
				lastSetTime.setTimeInMillis(cr.getTime());
			}

			btnSetTime.setOnClickListener(new View.OnClickListener() {
				@SuppressWarnings("deprecation")
				@Override
				public void onClick(View v) {
					showDialog(TIME_DIALOG_ID);
				}
			});

			btnCallCancelReminder.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					hideThisWindow();
				}
			});

			btnCallAcceptReminder.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					long nextReminder;
					if (tbMainRemindIn.isChecked()) {
						nextReminder = remindOptions.get(
								spHomeRemindTimes.getSelectedItem()) + System.currentTimeMillis();
					} else {
						nextReminder = lastSetTime.getTimeInMillis();
					}
					scheduleCall(nextReminder, cr);
					hideThisWindow();
					refresh();
				}
			});

			selectRemindIn();

			tbMainRemindAt.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					selectRemindAt();
				}
			});
			tbMainRemindIn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					selectRemindIn();
				}
			});
		}

		protected void selectRemindIn() {
			tbMainRemindIn.setChecked(true);
			tbMainRemindAt.setChecked(false);
			spHomeRemindTimes.setVisibility(View.VISIBLE);
			btnSetTime.setVisibility(View.GONE);
			btnCallAcceptReminder.setEnabled(true);
		}
		protected void selectRemindAt() {
			tbMainRemindIn.setChecked(false);
			tbMainRemindAt.setChecked(true);
			spHomeRemindTimes.setVisibility(View.GONE);
			btnSetTime.setVisibility(View.VISIBLE);
			btnCallAcceptReminder.setEnabled((lastSetTime != null));
		}
	}

	public void refresh() {
		onCheckedChanged(currentView, true);
		TelephonyService.updateScheduledCallNotification(MainActivity.this);
	}
	@Override
	protected Button getSetTimeButton() {
		return btnSetTime;
	}
	@Override
	protected Button getConfirmButton() {
		return btnCallAcceptReminder;
	}
}
